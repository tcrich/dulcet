<?php

namespace Drupal\dul_ingest\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drush\Commands\DrushCommands;
use Drush\Drush;
use Symfony\Component\Yaml\Yaml;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\block_content;
use Drupal\block_content\Entity\BlockContent;
use Drupal\block_content\Entity\BlockContentType;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\Core\Language\LanguageInterface;
use Drupal\file\Entity\File;
use Drupal\path_alias\Entity\PathAlias;
use Drupal\Core\Config\FileStorage;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\Entity\Term;
use Drupal\aggregator\Entity\Feed;
use Drupal\aggregator\Entity\Item;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;

function doBodyAdjustments(&$body_value, $source='node', &$data_report=NULL) {
  // this hash will allow us to keep up with detailed stats 
  // for 'bootstrap', 'fontAwesome', etc.
  // It'll be returned back to the caller

  $report = [];
  if ($data_report !== NULL) {
    //error_log("doBodyAdjustments | initializing 'report' hash...");
    $report = $data_report;
  }
  if (!isset($report[$source])) {
    //error_log("doBodyAdjustments | adding key (" . $source . ") to 'report' has...");
    $report[$source] = [];
  }

  $tmp = $body_value;

  // read the YAML file only once
  static $yamlData;
  if ($yamlData == NULL || $yamlData == '') {
    // error_log("doBodyAdjustments: reading /adjustments/main.yml...");
    $yamlData = file_get_contents(getenv('DUL_DRUPAL7_EXPORT_DIR') . '/adjustments/main.yml');
  }
  $yaml = new \Symfony\Component\Yaml\Yaml();
  $adjustments = $yaml->parse($yamlData);

  foreach ($adjustments as $key => $config) {
    // BAIL if the number of items in search and replace are not equal
    // ignore config sections that have a 'regex_match' flag
    if (!array_key_exists('regex_match', $config) && (count($config['search']) != count($config['replace']))) {
      error_log("doBodyAdjustments: error - unequal number of search/replace items in key (" . $key . ")!");
      return FALSE;
    }

    if (!isset($report[$source][$key])) {
      // error_log("doBodyAdjustments | initializing '" . $key . "' for report['" . $source . "']");
      $report[$source][$key] = 0;
    }

    $how_many_replaced = 0;
    if (array_key_exists('regex_match', $config)) {
      // match regex, then search/replace
      $tmp = preg_replace_callback(
        $config['match'],
        function($matches) use ($config) {
          return str_replace($config['search'], $config['replace'], $matches[0]);
        },
        $tmp,
        -1,
        $how_many_replaced
      );
    }
    else if (array_key_exists('regex', $config)) {
      // we'll call preg_replace here
      $tmp = preg_replace($config['search'], $config['replace'], $tmp, -1, $how_many_replaced);
    }
    else {
      // use str_replace
      $tmp = str_replace($config['search'], $config['replace'], $tmp, $how_many_replaced);
    }
    $report[$source][$key] += $how_many_replaced;
  }

  $body_value = $tmp;

  // if $data_report was provided by the 'caller', then send the data back...
  if ($data_report !== NULL) {
    $data_report = $report;
  }

  // we had a good run. :)
  return TRUE;
}

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class DulIngestCommands extends DrushCommands {

  /**
   * Parse menulinks file
   * 
   * @param $drup7_site
   *   Which drupal 7 site (perkins, rubenstein, etc)
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option prefix
   *   Prefix to add to the menu name
   * @usage dul_ingest-parseMenuLinks
   *
   * @command dul_ingest:parseMenuLinks
   * @aliases dul-menus
   */
  public function parseMenuLinks($drup7_site, $options = ['prefix' => '', 'link-prefix' => '']) {
    $jsonData = file_get_contents(getenv('DUL_DRUPAL7_EXPORT_DIR') . '/menulinks-' . $drup7_site . '.json');
    $tree = json_decode($jsonData);

    # make sure link-prefix has an '/' as the last character when present
    if ($options['link-prefix'] && !preg_match('/.+\/$/', $options['link-prefix'])) {
      $options['link-prefix'] .= '/';
    }

    foreach ($tree as  $menu_name => $menu_data) {
      if ($options['prefix'] != '') {
        $menu_name = $options['prefix'] . '_' . $menu_name;
      }

      $menu = \Drupal::entityTypeManager()
        ->getStorage('menu')
        ->load($menu_name);
      if ($menu == NULL) {
        $this->logger()->notice(sprintf("processing menu [%s] with %s item(s)", $menu_name, count($menu_data->links)));
        \Drupal::entityTypeManager()
          ->getStorage('menu')
          ->create([
            'id' => $menu_name,
            'label' => $menu_data->label,
            'description' => $menu_data->description,
          ])
          ->save();
      } else {
        //$this->logger()->notice("going to set label on existing menu to " . $menu_data['label']);
        $menu->set('label', $menu_data->label);
        $menu->set('description', $menu_data->description);
        $menu->save();
      } 
      $depth = 1;
      $parent_id = FALSE;
      $parents = array();

      foreach ($menu_data->links as $l) {
        $l = unserialize($l);
        if ($l['depth'] != $depth) {
          if ($l['depth'] < $depth) {
            array_shift($parents);
          }
          $depth = $l['depth'];
        }

        if (preg_match('/^<front>/', $l['link_path'])) {
          $link_uri = 'internal:/';
        } else {
          $link_uri = ($l['external'] == 0) ? sprintf("internal:/%s%s", $options['link-prefix'], $l['link_path']) : $l['link_path'];
        }
        $expanded = $l['expanded'] == 1;

        $edit = [
          'title' => $l['link_title'],
          'link' => ['uri' => $link_uri],
          'menu_name' => $menu_name,
          'weight' => $l['weight'],
          'expanded' => $expanded,
          'external' => $l['external'],
          'enabled' => $l['hidden'] ^ 1,
        ];
        if (!empty($parents)) {
          $edit['parent'] = $parents[0]; 
        }
        $menu_link = MenuLinkContent::create($edit);
        $menu_link->save();

        if ($l['has_children'] == 1) {
          array_unshift($parents, $menu_link->getPluginId());
        }

        # if link_path matches the 'node/<nid>' pattern
        # make a note of it
        $matches = [];
        if (preg_match('/^node\/(\d+)/', $l['link_path'], $matches) == 1) {
          file_put_contents('/opt/dul/reconcile/' . $drup7_site . '/node-' . $matches[1], $menu_link->id() . PHP_EOL, FILE_APPEND);
        }
      }
    }
  }

  /**
   * Import aggregator feeds from D7
   * 
   * @param $drup7_site
   *   Which drupal 7 site (perkins, rubenstein, etc)
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option prefix
   *   Prefix to add to the menu name
   * @usage dul_ingest-importFeeds
   *
   * @command dul_ingest:importFeeds
   * @aliases dul-feeds
   */
  function importFeeds($drup7_site, $options = ['prefix' => '']) {
    $jsonData = file_get_contents(getenv('DUL_DRUPAL7_EXPORT_DIR') . '/feeds-' . $drup7_site . '.json');
    $feeds = json_decode($jsonData);

    foreach ($feeds as $f) {
      $f = unserialize($f);
      if (!$f['title']) {
        continue;
      }
      $feed = \Drupal::entityTypeManager()->getStorage('aggregator_feed')
                                          ->create([
                                            'title' => $f['title'],
                                            'url' => $f['url'],
                                            'refresh' => $f['refresh'],
                                          ]);
      $feed->save();
      $feed->refreshItems();
    }
  }

  /**
   * Create user accounts for Web Editorial Board (WEB)
   * members
   * 
   * @usage dul_ingest-create-WebUsers
   * @command dul_ingest:createWebUsers
   * @aliases cwu,dul-webusers
   */
  public function createWebUsers($options = ['option-name' => 'default']) {
    // return samlauth module to be enabled
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('samlauth') == FALSE) {
      return;
    }

    $externalAuth = \Drupal::service('externalauth.externalauth');

    $role = \Drupal\user\Entity\Role::load('webeditor');
    if (!$role) {
      \Drupal\user\Entity\Role::create(['id' => 'webeditor', 'label' => 'Web Editorial Board'])->save();
    }

    $csvData = file_get_contents(getenv('DUL_DRUPAL7_EXPORT_DIR') . '/users/web.csv');
    $users = explode(PHP_EOL, $csvData);
    $this->logger()->notice(print_r($users, TRUE));
    foreach ($users as $user) {
      list($first_name, $last_name, $name) = explode(',', $user);
      if (!$name) {
        continue;
      }
      $user = $externalAuth->register($name . "@duke.edu", 'samlauth', ['name' => $name]);
      if ($user != NULL) {
        $user->set('first_name', $first_name);
        $user->set('last_name', $last_name);
        $user->addRole('webeditor');
        $user->save();
        $this->logger()->success(sprintf("Assigned 'webeditor' role to user: '%s %s' (%s)...", $first_name, $last_name, $name));
      }
    }
  }

  /**
   * Create user accounts for ITS Developers (its-developers)
   * and assign the role of administrator.
   *
   * Process the members provided in the DUL_ITS_DEVELOPERS 
   * environment variable
   *
   * @usage dul_ingest:createDevUsers
   * @command dul_ingest:createDevUsers
   * @aliases cdevs,dul-devusers
   */
  public function createDevUsers() {
    $devs = getenv('DUL_ITS_DEVELOPERS');
    if ($devs == FALSE) {
      return;
    }

    $moduleHandler = \Drupal::service('module_handler');
    $samlauthEnabled = $moduleHandler->moduleExists('samlauth');
    if ($samlauthEnabled) {
      $externalAuth = \Drupal::service('externalauth.externalauth');
    } else {
      $this->logger()->notice("[dev-devusers] 'samlauth' module is not enabled.");
      return;
    }

    foreach (preg_split("/[|;,]/", $devs) as $dev) {
      if ($samlauthEnabled) {
        // swiped from samlauth.module and Drupal/samlauth/SamlService
        $account = $externalAuth->register($dev . "@duke.edu", 'samlauth', ['name' => $dev]);
        if ($account != NULL) {
          $account->addRole('administrator');
          $account->save();
        }
      }
    }
  }

  /**
   * Create DUL-specific content-types.
   *
   * @option option-name
   *   Description
   * @usage dul_ingest-createDULTypes foo
   *   Usage description
   *
   * @command dul_ingest:createDULTypes
   * @aliases cdt,dul-ctypes
   */
  public function createDULTypes($options = ['option-name' => 'default']) {
    
    $yamlData = file_get_contents(getenv('DUL_DRUPAL7_EXPORT_DIR') . '/block_types.yml');
    $yaml = new \Symfony\Component\Yaml\Yaml();
    $bconfig = $yaml->parse($yamlData);
    
    foreach ($bconfig['block_types'] as $btype) {
      $b = BlockContentType::load($btype['id']);
      if ($b == NULL) {
        $b = BlockContentType::create($btype);
      }
      $b->save();
      block_content_add_body_field($b->id());
      $this->logger()->notice(sprintf("createDULTypes: created block type - %s", $btype['id']));
    }

    $form_display = \Drupal::entityTypeManager()->getStorage('entity_form_display')->load('user.user.default');
    $form_display->setComponent('first_name', ['type' => 'string_textfield', 'weight' => -51, 'settings' => ['max_length' => 60]])->save();
    $form_display->setComponent('last_name', ['type' => 'string_textfield', 'weight' => -50, 'settings' => ['max_length' => 60]])->save();
    $display = \Drupal::entityTypeManager()->getStorage('entity_view_display')->load('user.user.default');
    $display->setComponent('first_name', ['type' => 'string', 'weight' => -20, 'region' => 'content', 'label' => 'above'])->save();
    $display->setComponent('last_name', ['type' => 'string', 'weight' => -19, 'region' => 'content', 'label' => 'above'])->save();
  }

  /**
   * Command description here.
   *
   * @param $dataDir
   *   Directory where node data files (JSON) are located.
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option content-type
   *   Content type to assign to ingested nodes (default: article)
   * @option menu-prefix
   *   rl, data, or east
   * @usage dul_ingest-commandName foo
   *   Usage description
   *
   * @command dul_ingest:nodeIngest
   * @aliases node-in
   */
  public function nodeIngest($dataDir, 
                             $options = [
                                'content-type' => 'page',
                                'panel-groups' => FALSE,
                                'no-adjust' => FALSE, 
                                'reconcile-menulinks' => FALSE,
                                'drup7_site' => '',
                                'menu-prefix' => '']) {
    $be_verbose = FALSE;
    $be_verbose = getenv('DRUSH_VERBOSE') && (preg_match('/^(yes|YES|true|TRUE)/', getenv('DRUSH_VERBOSE')) == 1);
    
    if ($options['menu-prefix']) {
      $options['menu-prefix'] .= '_';
    }

    # these paths require attention when detected
    # (resolving aliases)
    $home_paths = [
      '/data/data-index' => '/data',
      '/rubenstein/index' => '/rubenstein',
    ];

    // Capture any failed ingest attempts for later data-correction
    // (hopefully not, however).
    $failed_ingests = [];
    $adjust_report = [];

    // Remove "." and ".." from the array of file entries
    $scanned_directory = array_diff(scandir($dataDir), array('..', '.'));

    // process the individual JSON files, each representing
    // an exported node, generated from a separate process.
    // --
    // TODO add standardized export data structure
    $total_files = count( $scanned_directory );
    $files_processed = 0;
    $nodes_with_layout = 0;
    $bootstrap_report = [];
    $this->logger()->notice(dt("Importing " . count($scanned_directory) . " nodes from " . $dataDir));

    $please_wait_msg = ($options['reconcile-menulinks']) ?
      "Please wait. This may take additional time to reconcile menu item links..." :
      "Please wait...";
    $this->logger()->notice($please_wait_msg);

    $bootstrap_replace_count = 0;
    foreach ($scanned_directory as $file) {
      try {
        // Decode as an associative array.
        // (json_decode returns 'stdClass' by default)
        $jsonData = file_get_contents($dataDir . '/' . $file);
        $node = json_decode($jsonData, TRUE, 512, JSON_THROW_ON_ERROR);

        $node_data = $node['node_field_data'];

        // Generate a new UUID in the absence of one.
        if (!array_key_exists('uuid', $node)) {
          $uuid = \Drupal::service('uuid');
          $new_uuid = $uuid->generate();
          $node['uuid'] = $new_uuid;
        }

        // Search for an existing node by its UUID
        // if it exists, update its body->value field 
        // as it's likely one we've manually adjusted.
        //
        // this section of code will also (while not originally planned) 
        // help to avoid UUID collisions.
        //
        // "continue" iteration when updated.
        $node_to_update = \Drupal::entityTypeManager()
          ->getStorage('node')
          ->loadByProperties(['uuid' => $node['uuid']]);
        if ($node_to_update) {
          // convert from array to object
          $node_to_update = reset($node_to_update);
          // update the body...
          $node_to_update->set('body', $node['node__body']['body_value']);
          try {
            $node_to_update->save();
          }
          catch (Exception $s) {
            $this->logger()->error("Unable to update node: " . $s);
          }

          // continue to the next file, instead of all the processing below.
          continue;
        }

        // Apply simple adjustments to the node body_value early
        // in the process...

        // make sure we have 'node__body'
        $adjusted_body_content = '';
        $body_format = 'full_html';
        if (isset($node['node__body'])) {
          $adjusted_body_content = $node['node__body']['body_value'];
          $body_format = $node['node__body']['body_format'];
        }
        if (!$options['no-adjust'] && !empty($adjusted_body_content)) {
          doBodyAdjustments($adjusted_body_content, 'node', $adjust_report);
        }

        $desired_content_type = isset($node['type']) ? $node['type'] : 'page';

        // For the main instance (Perkins or "DUL"), enforce the content-type 
        // of 'article', unless the node's type is already 'page'.
        //
        // In other instances (Rubenstein, Lilly, etc),
        // force the new type to be the one provided by the --content-type option
        if ($options['content-type'] != 'page') {
          $desired_content_type = $options['content-type'];
        } else {
          // Assume 'page', then change as needed
          $desired_content_type = 'page';
          $has_job_prefix = preg_match('/^\/about\/jobs\//', $node['path_alias']['alias']);
          if ($has_job_prefix == 1) {
            $desired_content_type = 'job_posting';
          }

          $has_exhibits_prefix = preg_match('/^\/exhibits\/\d+\//', $node['path_alias']['alias']);
          if ($has_exhibits_prefix) {
            $desired_content_type = 'exhibits_page';
          }
        }
        // THESE are the values to be saved with the 
        // soon-to-be created node.
        #$this->logger()->notice(sprintf("node status = [%s]\n", $node['node_field_data']['status']));
        if (!array_key_exists('status', $node['node_field_data'])) {
          $node['node_field_data']['status'] = 0;
        }
        if ($node['node_field_data']['status'] == NULL) {
          $node['node_field_data']['status'] = 0;
        }
        $edit = array(
          // set properties.
          // however, do not set the nid from the original D7 node
          // as to avoid collision with data from rubenstein, datagis and east nodes
          //'nid' => $node['nid'],
          //'revision' => $node['vid'],
          'title' => $node['node_field_data']['title'],
          'status' => $node['node_field_data']['status'],
          'type' => $desired_content_type,
          'body' => ['value' => $adjusted_body_content, 'format' => $body_format],
          'sticky' => $node_data['sticky'],
          'promote' => $node_data['promote'],
          'uuid' => $node['uuid'],
        );

        // Currently, asidebox data is mapping to the existing fields created
        // from the config import
        // ASIDEBOX -- when an asidebox is detected, do a couple of things:
        // 1) transfer the content to the "field_aside_box" field
        // 2) create an 'asidebox' block
        if (key_exists('asidebox', $node) && $node['asidebox'] != null) {
          $adjusted_asidebox_content = $node['asidebox']['body_value'];
          doBodyAdjustments($adjusted_asidebox_content, 'asidebox', $adjust_report);
          //$asidebox_block_body = adjustedBody($node['asidebox']['body_value'], $bootstrap_replace_count);

          $edit["field_aside_box"] = ['value' => $adjusted_asidebox_content, 'format' => $node['asidebox']['body_format']];
          if ($node['asidebox']['body_title']) {
            $edit['field_aside_box_title'] = $node['asidebox']['body_title'];
          }
          # $this->logger()->success(dt("Transferred aside box content to node fields"));
          
          // CREATE BLOCK now
          $block_info = $node['asidebox']['body_title'] == "" 
            ? "[Asidebox] " . $node['node_field_data']['title'] 
            : "[Asidebox] " . $node['asidebox']['body_title'];
            
          if (empty($node['asidebox']['body_value'])) {
            $block_info .= ' (empty)';
          }

          $asidebox_block = BlockContent::create([
            'info' => $block_info,
            'reusable' => TRUE,
            'type' =>'asidebox',
            'body' => ['value' => $adjusted_asidebox_content, 'format' => $node['asidebox']['body_format']],
            'langcode' => 'en',
          ]);
          try {
            $asidebox_block->save();
          } catch (Exception $exc) {
            $this->logger()->error($exc->getMessage());
          }
        }

        // EXHIBITS fields
        // add exhibits fields when detected.
        //
        // To understand how the exhibit field data is exported, 
        // see here:
        // https://gitlab.oit.duke.edu/dlc32/drupal-data-export/-/blob/master/Exhibits.pm
        if (key_exists('exhibit', $node)) {
          foreach ($node['exhibit'] as $field_key => $data) {
            if ($field_key == 'field_opening_date') {
              // We need to apply some care to the date field.
              // In our Drupal 7 instance, the end value was 'value2'.
              // In our upcoming D9 instance, we're using 'end_value'
              //
              // replace 'value2' with 'end_value'
              //
              // We also need to remove the 'time' portion of the datetime
              // value so the display field will function correctly.
              $data['value'] = (explode(' ', $data['value']))[0];
              $data['end_value'] = (explode(' ', $data['value2']))[0];

              // when we've done all of that, then remove 'value2' from the 
              // hash.
              unset($data['value2']);
            }

            // store the value in our "edit" dictionary
            $edit[$field_key] = $data;
          }
        } 

        // create the new node object based on the information we have 
        // so far.
        $new_node = Node::create($edit);

        // Attempt to save the node.
        // In the rare event of a UUID collision, move on to the next 
        // entry.
        try {
          $new_node->save();
        } catch(Exception $e) {
          $this->logger()->warning(dt("Couldn't save this node: " . $edit['uuid']));
          continue;
        }

        $nid = $new_node->id();
        $be_verbose && $this->logger()->success(dt('Created node for nid: ' . $nid));
        // END node creation

        // PATH ALIAS
        // create the path_alias entry for this node
        $alias = $node['path_alias']['alias'];
        if ($alias == '/rubenstein/index' || $alias == '/data/data-index') {
          // At some point we can remove this message...
          $this->logger()->notice(sprintf("Found alias that's an index page... [%s]", $alias));
          if (isset($home_paths[$alias])) {
            $alias = $home_paths[$alias];
          }
        }
        $path_alias = PathAlias::create([
          'path' => '/node/' . $nid,
          'alias' => $alias
        ])->save();

        # Save the node again so the alias saved above will be used to
        # update the calculated field "Site Section"
        $new_node->save();
        $be_verbose && $this->logger()->success(dt('Added path alias: ') . $node['path_alias']['alias']);
        // END PATH ALIAS

        // MENU LINK CONTENT reconciliation
        if ($options['reconcile-menulinks']) {
          if (!isset($options['drup7_site']) || empty($options['drup7_site'])) {
            $this->logger()->error("option 'drup7_site' needs to be set when reconciling menu items...");
            continue;
          }
          $fname = sprintf("/opt/dul/reconcile/%s/node-%s", $options['drup7_site'], $node['nid']);
          if (file_exists($fname)) {
            $menulink_ids = explode(PHP_EOL, rtrim(file_get_contents($fname)));
            foreach ($menulink_ids as $menulink_id) {
              $menu_link_content = MenuLinkContent::load($menulink_id);
              if ($menu_link_content != NULL) {
                $menu_link_content->link->uri = "internal:/node/" . $new_node->id();
                $menu_link_content->save();
              } else {
                $this->logger()->notice("Looked for menu link content, but couldn't find it...");
              }
            }
          }
        }

        // ASSIGNED IMAGES (field_image)
        // -----------------------------
        // Does this node have an assigned image?
        // PRO TIP:
        // https://drupal.stackexchange.com/questions/287997/how-do-i-programmatically-attach-a-file-to-a-file-field-in-a-node
        if (key_exists('field_image', $node) && $node['field_image'] != null) {

          $query = \Drupal::entityQuery('file')
            ->condition('uuid', $node['field_image']['file_uuid']);

          $results = $query->execute();
          $target_fid = array_pop($results);
          if ($target_fid) {
            $new_node->set('field_image', $target_fid);
            $new_node->save();
            $be_verbose && $this->logger()->notice(sprintf("attaching image (fid=%s) to node [%s]", $target_fid, $node['node_field_data']['title']));
            $be_verbose && $this->logger()->success(dt('Added image to the node...'));
          }

          $files_processed++;
        }
        // END ASSIGNED IMAGES
        
        // LAYOUT BUILDER SUPPORT (SECTIONS!)
        // TIP SOURCE: https://www.droptica.com/blog/layout-builder-building-drupal-8-layouts/
        //
        // This section of code is typically executed for 
        // former Drupal-7 panel pages
        $ignored_blocks = [];
        if (isset($node['section_components']) && $node['section_components']) {
          // Use this space to:
          // 1) Create inline blocks, or preserving relationship between block reference
          // 2) Add sections to this layout
          $section = new Section('asidebox_option');
          foreach ($node['section_components'] as $c) {
            // This use case is for original custom panel content from 
            // our DUL D7 site(s)

            $component_uuid = FALSE;
            $pluginConfiguration = FALSE;
            if (key_exists('provider', $c['configuration']) && $c['configuration']['provider'] == 'layout_builder') {
              # create an inline (non-reusable) block, save it, 
              # get its revision id
              
              $pane_config = unserialize($c['block_content']['content']['configuration']);
              if (is_array($pane_config)) {
                $uuid = \Drupal::service('uuid');
                $new_uuid = $uuid->generate();

                $adjusted_panel_content = '';
                $pane_body_format = 'full_html';
                if (key_exists('format', $pane_config)) {
                  $pane_config['format'] = str_replace('_no_wysiwyg', '', $pane_config['format']);
                  $pane_body_format = $pane_config['format'];
                }
                if (key_exists('body', $pane_config)) {
                  $adjusted_panel_content = $pane_config['body'];
                  doBodyAdjustments($adjusted_panel_content, 'panel_content', $adjust_report);
                }

                $custom_block = BlockContent::create([
                  // make sure the block does not appear in the library
                  'reusable' => FALSE,
                  // language set to en
                  'langcode' => 'en',
                  'body' => [
                    'value' => $adjusted_panel_content,
                    'format' => $pane_body_format,
                  ],
                  // set the block type (most likely basic)
                  'type' => 'basic',
                  // UUID
                  'uuid' => $new_uuid,
                  // INFO (title) that appears in the 
                  // custom block library
                  'info' => $pane_config['admin_title'],
                ]);
                $custom_block->save();
                
                $pluginConfiguration = [
                  'id' => 'inline_block:basic',
                  'provider' => 'layout_builder',
                  'label_display' => FALSE,
                  'view_mode' => 'full',
                  'block_revision_id' => $custom_block->revision_id->value,
                ];

                // $component = new SectionComponent($custom_block->uuid(), 'contentmain', $pluginConfiguration);
                $component = new SectionComponent(\Drupal::service('uuid')->generate(), 'contentmain', $pluginConfiguration);
                $section->appendComponent($component);

                $be_verbose && $this->logger()->notice("Added inline block for " . $custom_block->uuid());
                $be_verbose && $this->logger()->notice("");
              }
            } 
            // This case when an original D7 panel pane references a block
            // Attempt to load the block, but bail if $block returns null.
            //
            // A null $block is possible if the referenced block is not a 
            // D7 'block_custom' block -- as we're currently not migrating those over
            else if (key_exists('provider', $c['configuration']) && $c['configuration']['provider'] == 'block_content') {
              // load the existing block by its id, get its UUID and add to section
              $bid = $c['storage_bid'];
              
              if (preg_match('/^menu-/', $bid) == 1) {
                $menu = \Drupal::entityTypeManager()
                  ->getStorage('menu')
                  ->load($options['menu-prefix'] . preg_replace('/^menu-/', '', $bid, 1));

                if ($menu == NULL) {
                  continue;
                }        
                
                $pluginConfiguration = [
                  'id' => 'system_menu_block:' . $menu->id(),
                  'provider' => 'system',
                  'label_display' => FALSE,
                ];
                $component_uuid = $menu->uuid();
              } else {
                $fname = sprintf("/opt/dul/reconcile/%s/block-%s", $options['drup7_site'], $bid);
                if (file_exists($fname)) {
                  $block_uuid = array_pop(explode(PHP_EOL, rtrim(file_get_contents($fname))));
                  $blocks = \Drupal::entityTypeManager()->getStorage('block_content')->loadByProperties(['uuid' => $block_uuid]);
                  if (empty($blocks)) {
                    $this->logger()->notice("!!! the blocks are empty...");
                    $ignored_blocks[] = $c['pane_subtype'];
                    continue;
                  }
                  $block = array_shift($blocks);
                  $component_uuid = $block->uuid();
                  # otherwise, proceed with this block
                  $pluginConfiguration = [
                    'id' => 'block_content:' . $block->uuid(),
                    'provider' => 'block_content',
                    'label_display' => FALSE,
                    'view_mode' => 'full',
                    'status' => 1,
                    'context_mapping' => [],
                  ];
                }
              }
              if ($pluginConfiguration) {
                $component = new SectionComponent(\Drupal::service('uuid')->generate(), 'contentmain', $pluginConfiguration);
                $section->appendComponent($component);
              }
            }
          }
          
          //$new_node->layout_builder__layout->setValue($section);
          $new_node->get('layout_builder__layout')->setValue($section);
          // denote nodes with pre-filled layout with an asterisk
          $new_node->setTitle($node['node_field_data']['title']);
          $new_node->save();

          $nodes_with_layout++;
        }
        // END SECTIONS

        // Show us the block names of the ones skipped over
        // (because they were not 'block_custom' blocks)
        if (count($ignored_blocks) > 0) {
          $this->logger()->notice(print_r(array('IGNORED_BLOCKS' => $ignored_blocks), TRUE));
        }

      } catch(Exception $e) {
        $this->logger()->error("Error processing content: " . $node['node_field_data']['title]']);
        array_push($failed_ingests, [ 'nid' => $node['nid'], 'vid' => $node['vid']]);
        continue;
      }
    }
    $this->logger()->notice("");
    $this->logger()->notice("nodeIngest: Body HTML adjustments:");
    $this->logger()->notice("----------------------------------");
    $this->logger()->notice(print_r($adjust_report, TRUE));
    $this->logger()->notice("");

    if ($nodes_with_layout > 0) {
      $this->logger()->notice(sprintf("%s 'layout-aware' nodes were successfully imported", $nodes_with_layout));
    }

    $this->logger()->notice("Import done.");
  }

  /**
   * Ingest Custom Blocks from batch of JSON files
   *
   * @param $dataDir
   *   Directory where JSON files are located
   * @param array $options
   *   Associative array of options
   * @option block-type
   *   Assign block to this type
   * @option title-prefix
   *   Process blocks assigned to this theme
   * @usage dul_ingest:blocksIngest
   *   Usage description
   *
   * @command dul_ingest:blocksIngest
   * @aliases blocks-in
   */
  public function blockIngest($dataDir, $options = ['block-type' => '', 'title-prefix' => '', 'drup7_site' => '']) {
    // Capture any failed ingest attempts for later data-correction
    // (hopefully not, however).
    $failed_ingests = [];
    $adjust_report = [];

    // Remove "." and ".." from the array of file entries
    $scanned_directory = array_diff(scandir($dataDir), array('..', '.'));

    // process the individual JSON files, each representing
    // an exported node, generated from a separate process.
    // --
    // TODO add standardized export data structure
    $blocks_created = 0;
    $this->logger()->notice(dt("Processing imported blocks. Please wait..."));
    $bootstrap_replace_count = 0;
    foreach ($scanned_directory as $file) {
      try {
        // Decode as an associative array.
        // (json_decode returns 'stdClass' by default)
        $jsonData = file_get_contents($dataDir . '/' . $file);

        // If there's a %UUID% placeholder in the JSON, generate a
        // value and replace it. If there's an existing uuid, just
        // leave it. We may need the UUIDs to be stable for any custom
        // block that has a corresponding config file for its
        // region placement, since the YML references the block by UUID.

        if (preg_match('/%UUID%/', $jsonData)) {
          // before decoding, create a UUID and substitute all
          // occurrences of '%UUID%' with this new value.
          $uuid = \Drupal::service('uuid');
          $new_uuid = $uuid->generate();
          $jsonData = str_replace("%UUID%", $new_uuid, $jsonData);
        }

        $block = json_decode($jsonData, TRUE, 512, JSON_THROW_ON_ERROR);

        $block_info = $block['content']['info'];
        if ($options['title-prefix'] != '') {
          $block_info = sprintf("[%s] %s", $options['title-prefix'], $block['content']['info']);
        }

        $adjusted_block_content = $block['content']['body']['value'];
        doBodyAdjustments($adjusted_block_content, 'block', $adjust_report);

        $block_content_type = $options['block-type'] ? $options['block-type'] : $block['content']['type'];
        $new_block_data = [
          // make sure the blocks appear in the library
          'reusable' => TRUE,
          // language set to en
          'langcode' => $block['content']['langcode'],
          'body' => [
            'value' => $adjusted_block_content,
            'format' => 'full_html',
            #$block['content']['body']['format'],
          ],
          // set the block type (most likely basic)
          'type' => $block_content_type,
          // UUID
          'uuid' => $block['content']['uuid'],
          // INFO (title) that appears in the 
          // custom block library
          'info' => $block_info,
        ];

        // preserve the block id (bid) if present
        if (isset($block['bid']) && $block['bid']) {
          $new_block_data['bid'] = $block['bid'];
        }
        $custom_block = BlockContent::create($new_block_data);

        try {
          #$this->logger()->notice("saving block: " . $block_info . "...");
          $custom_block->save();
          
          // to help with reconciling block content when importing panel pages...
          if ($options['drup7_site'] && isset($block['bid'])) {
            file_put_contents('/opt/dul/reconcile/' . $options['drup7_site'] . '/block-' . $block['bid'], $custom_block->uuid() . PHP_EOL, FILE_APPEND);
          }
          
          $blocks_created++;
        } catch (Exception $exc) {
          $this->logger()->error("Unable to save custom block: " . $exc);
          $failed_ingest[] = $block;
        }
      } catch(Exception $e) {
      }
    }
    $this->logger()->notice("");
    $this->logger()->notice("blocksIngest: Body HTML adjustments:");
    $this->logger()->notice("------------------------------------");
    $this->logger()->notice(print_r($adjust_report, TRUE));
    $this->logger()->notice("");
    $this->logger()->notice(dt(sprintf("%s blocks created successfully...", $blocks_created)));
  }

  /**
   * Ingest Custom Blocks from batch of JSON files
   *
   * @param $dataFile
   *   Directory where JSON files are located
   * @usage dul_ingest:asideboxBlockImport
   *   Usage description
   *
   * @command dul_ingest:asideboxBlockImport
   * @aliases abox-import,abox-in
   */
  public function asideboxBlockImport($dataFile, $options = ['menu-prefix' => '']) {
    // This command has to run AFTER nodes have initially been imported
    // That way, we can load a node, and add these blocks as section components
    // 
    // Capture any failed ingest attempts for later data-correction
    // (hopefully not, however).
    $failed_ingests = [];

    // Remove "." and ".." from the array of file entries
    try {
      $jsonData = file_get_contents($dataFile);
    } catch(Exception $fe) {
      $this->logger()->fatal($fe);
    }
    
    $blocks = unserialize(json_decode($jsonData));
    foreach ($blocks as $b) {
      // before decoding, create a UUID and substitute all
      // occurrences of '%UUID%' with this new value.
      $uuid = \Drupal::service('uuid');
      $new_uuid = $uuid->generate();
      $uuid_for_attached_node = $b['node_uuid'];
      
      $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['uuid' => $uuid_for_attached_node]);
      if (empty($nodes)) {
        continue;
      }
      $node = array_shift($nodes);

      $section = new Section('asidebox_option');
      $what_was_attached = '';
      if (isset($b['menu_name'])) {
        $menu = \Drupal::entityTypeManager()
          ->getStorage('menu')
          ->load($options['menu-prefix'] . preg_replace('/^menu-/', '', $b['menu_name'], 1));
          
        if ($menu != NULL) {
          $this->logger()->notice("Found menu for '" . $b['menu_name'] . "', with UUID of '" . $menu->uuid() . "'");
        } else {
          continue;
        }
        $component_uuid = $menu->uuid();
        $pluginConfiguration = [
          'id' => 'system_menu_block:' . $menu->id(),
          'provider' => 'system',
          'label_display' => 0,
          #'context_mapping' => [
          #  'entity' => 'layout_builder.entity',
          #  'view_mode' => 'view_mode',
          #],
        ];
        $component_uuid = $menu->uuid();
        $what_was_attached = "menu block";
      } else {
        // convert as many Bootstrap 3 rules to Bootstrap 4
        // as we can.
        $adjust_report = [];
        $blocks_created = 0;
        $adjusted_body = $b['content']['body']['value'];
        doBodyAdjustments($adjusted_body, 'asidebox', $adjust_report);

        $custom_block = BlockContent::create([
          'bid' => $b['bid'],
          // make sure the blocks appear in the library
          'reusable' => TRUE,
          // language set to en
          'langcode' => $b['content']['langcode'],
          'body' => [
            'value' => $adjusted_body,
            'format' => $b['content']['body']['format'],
          ],
          'type' => 'asidebox',
          'uuid' => $new_uuid,
          'info' => sprintf("[Module Block][%s] %s", $b['content']['storage_module'], $b['content']['info']),
        ]);

        try {
          $custom_block->save();
          $blocks_created++;
        } catch (Exception $exc) {
          $this->logger()->error("Unable to save custom block: " . $exc);
          $failed_ingest[] = $block;
          continue;
        }
        # otherwise, proceed with this block
        $component_uuid = $custom_block->uuid();
        $pluginConfiguration = [
          'id' => 'block_content:' . $custom_block->uuid(),
          'provider' => 'block_content',
          'label_display' => 0,
          'view_mode' => 'full',
          #'context_mapping' => [
          #  'entity' => 'layout_builder.entity',
          #  'view_mode' => 'view_mode',
          #],
        ];
        $what_was_attached = 'custom block';
      }
      $component = new SectionComponent($component_uuid, 'asidebox', $pluginConfiguration);
      $section->appendComponent($component);

      // add the body field to the 'contentmain' region'
      $pluginConfiguration = [
        'id' => 'field_block:node:page:body',
        'view_mode' => 'full',
        'label_display' => 0,
        'context_mapping' => ['entity' => 'layout_builder.entity'],
        'formatter' => [
          'label' => 'hidden',
          'type' => 'text_default',
          'settings' => [],
          'third_party_settings' => [],
        ],
      ];
      $component = new SectionComponent(\Drupal::service('uuid')->generate(), 'contentmain', $pluginConfiguration);
      $section->appendComponent($component);

      $node->layout_builder__layout->setValue($section);
      $node->save();
      # $this->logger()->notice(sprintf("asideboxBlockImport | Node %s (%s), UUID %s layout -- added '%s'", $node->id(), $node->getTitle(), $b['node_uuid'], $what_was_attached));
       
      //if ($layout != NULL) {
      //  $section = new Section('asidebox_option');
      //  $component = new SectionComponent($new_uuid, 'contentmain', $pluginConfiguration);
      //  $section->appendComponent($component);
      //  $node->layout_builder__layout->setValue($section);
      //  $node->save();
      //}
    }
    # $this->logger()->notice(dt(sprintf("%s blocks created successfully...", $blocks_created)));
  }

  /**
   * Sample command for now
   *
   * @param $dataDir
   *   Directory where JSON files are located
   * @param array $options
   *   An associative array of options
   * @option option-name
   *   Description
   * @usage dul_ingest:sampleIngest foo
   *   Usage description
   *
   * @command dul_ingest:sampleIngest
   * @aliases pages-ingest,pages-in,pp-in
   */
  public function sampleIngest($dataDir, $options = ['option-name' => 'default']) {
    $be_verbose = FALSE;
    $be_verbose = $_ENV['DRUSH_VERBOSE'] && (preg_match('/^(yes|YES|true|TRUE)/', $_ENV['DRUSH_VERBOSE']) == 1);
    
    /*
    $mainSection = \Drupal\layout_builder\Section::new
    $serialized = 'O:29:"Drupal\layout_builder\Section":4:{s:11:" * layoutId";s:15:"asidebox_option";s:17:" * layoutSettings";a:2:{s:5:"label";s:0:"";s:15:"context_mapping";a:0:{}}s:13:" * components";a:4:{s:36:"2de9bd6b-5704-4689-9f0f-071df412282a";O:38:"Drupal\layout_builder\SectionComponent":5:{s:7:" * uuid";s:36:"2de9bd6b-5704-4689-9f0f-071df412282a";s:9:" * region";s:11:"contentmain";s:16:" * configuration";a:6:{s:2:"id";s:26:"field_block:node:page:body";s:5:"label";s:4:"Body";s:8:"provider";s:14:"layout_builder";s:13:"label_display";s:1:"0";s:9:"formatter";a:4:{s:5:"label";s:6:"hidden";s:4:"type";s:12:"text_default";s:8:"settings";a:0:{}s:20:"third_party_settings";a:0:{}}s:15:"context_mapping";a:2:{s:6:"entity";s:21:"layout_builder.entity";s:9:"view_mode";s:9:"view_mode";}}s:9:" * weight";i:0;s:13:" * additional";a:0:{}}s:36:"8c780329-f84e-45c2-a5c1-e4e55ed43d58";O:38:"Drupal\layout_builder\SectionComponent":5:{s:7:" * uuid";s:36:"8c780329-f84e-45c2-a5c1-e4e55ed43d58";s:9:" * region";s:14:"asideboxheader";s:16:" * configuration";a:6:{s:2:"id";s:43:"field_block:node:page:field_aside_box_title";s:5:"label";s:15:"Aside Box Title";s:8:"provider";s:14:"layout_builder";s:13:"label_display";s:1:"0";s:9:"formatter";a:4:{s:5:"label";s:6:"hidden";s:4:"type";s:12:"text_default";s:8:"settings";a:0:{}s:20:"third_party_settings";a:0:{}}s:15:"context_mapping";a:2:{s:6:"entity";s:21:"layout_builder.entity";s:9:"view_mode";s:9:"view_mode";}}s:9:" * weight";i:0;s:13:" * additional";a:0:{}}s:36:"9cee294b-e7b8-40d6-883b-164e97016081";O:38:"Drupal\layout_builder\SectionComponent":5:{s:7:" * uuid";s:36:"9cee294b-e7b8-40d6-883b-164e97016081";s:9:" * region";s:8:"asidebox";s:16:" * configuration";a:6:{s:2:"id";s:37:"field_block:node:page:field_aside_box";s:5:"label";s:9:"Aside Box";s:8:"provider";s:14:"layout_builder";s:13:"label_display";s:1:"0";s:9:"formatter";a:4:{s:5:"label";s:6:"hidden";s:4:"type";s:12:"text_default";s:8:"settings";a:0:{}s:20:"third_party_settings";a:0:{}}s:15:"context_mapping";a:2:{s:6:"entity";s:21:"layout_builder.entity";s:9:"view_mode";s:9:"view_mode";}}s:9:" * weight";i:0;s:13:" * additional";a:0:{}}s:36:"6b9deb29-9649-4745-8e1e-51107c1c4200";O:38:"Drupal\layout_builder\SectionComponent":5:{s:7:" * uuid";s:36:"6b9deb29-9649-4745-8e1e-51107c1c4200";s:9:" * region";s:11:"contentmain";s:16:" * configuration";a:8:{s:2:"id";s:18:"inline_block:basic";s:5:"label";s:14:"Custom Block 1";s:8:"provider";s:14:"layout_builder";s:13:"label_display";s:7:"visible";s:9:"view_mode";s:4:"full";s:17:"block_revision_id";s:3:"146";s:16:"block_serialized";N;s:15:"context_mapping";a:0:{}}s:9:" * weight";i:1;s:13:" * additional";a:0:{}}}s:21:" * thirdPartySettings";a:0:{}}';

    $section = unserialize($serialized);
    $this->logger()->notice(print_r($section, true));
    */
  }

  /**
   * Ingest File Managed entries from batch of JSON files
   *
   * @param $dataDir
   *   Directory where file_managed JSON files are located
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option option-name
   *   Description
   * @usage dul_ingest-taxonomyIngest foo2
   *   Usage description
   *
   * @command dul_ingest:fileManagedIngest
   * @aliases file-in
   */
  public function fileManagedIngest($dataDir, $options = ['option-name' => 'default']) {
    $be_verbose = FALSE;
    $be_verbose = getenv('DRUSH_VERBOSE') && (preg_match('/^(yes|YES|true|TRUE)/', getenv('DRUSH_VERBOSE')) == 1);

    $failed_ingests = [];

    // Remove "." and ".." from the array of file entries
    $scanned_directory = array_diff(scandir($dataDir), array('..', '.'));

    // process the individual JSON files, each representing
    // an exported node, generated from a separate process.
    // --
    // TODO add standardized export data structure
    $this->logger()->notice("Importing " . count($scanned_directory) . " managed files from " . $dataDir . "...");
    foreach ($scanned_directory as $file) {
      try {
        // Decode as an associative array.
        // (json_decode returns 'stdClass' by default)
        $jsonData = file_get_contents($dataDir . '/' . $file);
        $file_managed = json_decode($jsonData, TRUE, 512, JSON_THROW_ON_ERROR);

        // adjust the URI when "/exhibits" is the leading slug
        if (preg_match('/public:\/\/exhibits/', $file_managed['uri'])) {
          $file_managed['uri'] = str_replace('exhibits/', 'dul/exhibits/', $file_managed['uri']);
        }

        $edit = array(
          'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,

          // preserve the original UUID
          'uuid' => $file_managed['uuid'],

          'filename' => $file_managed['filename'],
          'uri' => $file_managed['uri'],
          'filemime' => $file_managed['filemime'],
          'filesize' => $file_managed['filesize'],
          'status' => $file_managed['status']
        );

        $new_file = File::create($edit);
        $new_file->save();
        $fid = $new_file->id();
        $be_verbose && $this->logger()->success(dt('Created managed file: ') . $file_managed['filename']);
        
      } catch(Exception $e) {
        array_push($failed_ingests, [ 'uuid' => $file_managed['uuid'], 'uri' => $file_managed['uri'] ]);
        continue;
      }
    }
    $this->logger()->notice("Import done.");

    $query = \Drupal::entityQuery('file')
      ->condition('uuid', '11a69f73-9fe2-4777-a408-fe597ed25562')
      ->addTag('debug');
    $r = $query->execute();

    // rebuild the cache
    $alias_manager = Drush::service('site.alias.manager');
    Drush::drush($alias_manager->getSelf(), 'cache-rebuild')->run();
    $this->logger()->success(dt("Cache rebuilt"));

  }

  /**
   * Find node by UUID and debug
   *
   * @param $uuid
   *   UUID to query by
   * @usage dul_ingest-nodeQuery UUID
   *   Usage description
   *
   * @command dul_ingeust:nodeQuery
   * @aliases nodeq,nq
   */
  public function nodeQuery($uuid) {
    //$node = \Drupal::entityTypeManager()->getStorage('node')->loadRevision($b['vid']);
    $node = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['uuid' => $uuid]);
    //$node = array_pop($node);
    //$layout = $node->get('layout_builder__layout');
    $this->logger()->notice(print_r($node, TRUE));
  }
  
  /**
   * Ingest Taxonomy Vocabulary entries from batch of JSON files
   *
   * @param $dataDir
   *   Directory taxonomy-related JSON files are located
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option option-name
   *   Description
   * @usage dul_ingest-taxonomyIngest foo2
   *   Usage description
   *
   * @command dul_ingest:taxonomyIngest
   * @aliases tax-in
   */
  public function taxonomyIngest($dataDir, $options = ['option-name' => 'default']) {
    #$config_path  = $yamlDir . '/vocabulary.yaml';

    #$jsonData = file_get_contents($dataDir . 'vocabulary.json');
    #$vocab_json = json_decode($jsonData, TRUE, 512, JSON_THROW_ON_ERROR);

    // Create 'Library Content' Vocabulary
    $yamlData = file_get_contents(getenv('DUL_DRUPAL7_EXPORT_DIR') . '/carousel_tags.yml');
    $yaml = new \Symfony\Component\Yaml\Yaml();
    $tags = $yaml->parse($yamlData);
    
    foreach ($tags['tags'] as $UUID => $tag_name) {
      \Drupal\taxonomy\Entity\Term::create([
        'name' => $tag_name,
        'uuid' => $UUID,
        'vid' => 'tags'
      ])->save();
    }

    // Create Carousel Tag
    #\Drupal\taxonomy\Entity\Term::create([
    #  'name' => 'Carousel: RL Hartman Home',
    #  'uuid' => '3b0bb979-cdf4-4484-8f28-19db36205b75',
    #  'vid' => 'tags'
    #])->save();

    /*
    $vid = 'library_content';
    \Drupal::entityTypeManager()->getStorage('taxonomy_vocabulary')
                                ->create([
                                  'vid' => $vid,
                                  'name' => dt('Library Content'),
                                  'description' => dt('Library content terms'),
                                ])->save();
    $termdefs = array(
      array('name' => 'Perkins', 'description' => ['value' => 'DUL Content'], 'alias' => '',),
      array('name' => 'Rubenstein', 'description' => ['value' => 'Rubenstein Content'], 'alias' => '/rubenstein',),
      array('name' => 'CDVS', 'description' => ['value' => 'CDVS Content'], 'alias' => '/cdvs',),
      array('name' => 'Lilly', 'description' => ['value' => 'Lilly Content'], 'alias' => '/lilly',),
      array('name' => 'Music', 'description' => ['value' => 'Music Content'], 'alias' => '/music',),
    );

    $weight = 0;
    foreach ($termdefs as $t) {
      $new_term = \Drupal\taxonomy\Entity\Term::create([
        'name' => $t['name'],
        'vid' => $vid,
        'weight' => $weight++,
      ]);
      $new_term->save();
      $this->logger->notice(dt("Created term: " . $new_term->getName()));

      if ($t['alias']) {
        // PATH ALIAS
        // create the path_alias entry for this node
        $path_alias = PathAlias::create([
          'path' => '/taxonomy/term/' . $new_term->id(),

          # NOTE: make sure we have the appropriate prefix for
          # this URL (e.g. rubenstein, music, lilly, datavis)
          # when it's not from the main 'perkins' instance.
          'alias' => $t['alias'],
        ])->save();
      }
    }
    */

    #$alias_manager = Drush::service('site.alias.manager');
    #Drush::drush($alias_manager->getSelf(), 'cache-rebuild')->run();
    #$this->logger()->success(dt("Cache rebuilt"));
  }
}
