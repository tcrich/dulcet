<?php
namespace Drupal\dul_ingest\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides route responses for the dul_ingest module.
 */
class DulIngestController extends ControllerBase {

  /**
   * Return a list of nodes whose body field
   * contains Google Analytics markup.
   *
   * @return array
   *  A simple render array
   */
  public function gaNodes() {
    // When the current user has permission to 'administer blocks'
    // we'll create a URL link for the block entries below.
    $user_has_permission = \Drupal::currentUser()->hasPermission('administer blocks');

    $page = array();

    $query = \Drupal::entityQuery('node');
    $group = $query
      ->orConditionGroup()
      ->condition('body', '%_gaq.push%', 'LIKE')
      ->condition('body', '%ga(\'send\'%', 'LIKE');
    $query->condition($group)
      ->accessCheck(FALSE)
      ->sort('created', 'DESC');
    $results = $query->execute();

    $nodes = \Drupal\node\Entity\Node::loadMultiple($results);
    $node_rows = [];
    foreach ($nodes as $nid => $n) {
      $node_rows[] = [ $nid, $n->getTitle() ];
    }

    $entity_discovery_message = "<div><p>We discovered %s %s with <ul><li><code>onclick(\"ga('send', ...))</code></li>"
      . "<li><code>_gaq.push</code></li></ul></p></div>";

    $node_count = count($nodes);
    $page[] = [
      '#title' => t('Nodes Resuls'),
      '#markup' => sprintf($entity_discovery_message, 'nodes', $node_count),
    ];
    $page[] = [
      '#title' => t('Affected Nodes'),
      '#type' => 'table',
      '#header' => [
        'col1' => t('ID'),
        'col2' => t('Title'),
      ],
      '#rows' => $node_rows,
    ];

    $block_content_storage = \Drupal::entityTypeManager()->getStorage('block_content');
    $query = $block_content_storage->getQuery();
    $or = $query
      ->orConditionGroup()
      ->condition('body', '_gaq.push', 'CONTAINS')
      ->condition('body', 'ga(\'send\'', 'CONTAINS');
    $query->condition($or)
      ->accessCheck(FALSE)
      ->sort('changed', 'DESC');


    $results = $query->addTag('debug')->execute();
    $blocks = $block_content_storage->loadMultiple($results);
    $block_rows = [];
    foreach ($blocks as $bid => $b) {
      $row_data = [];
      $block_rows[] = [];
      if ($user_has_permission) {
        $row_data[] = Link::createFromRoute( t('Edit'), 'entity.block_content.canonical', ['block_content' => $bid] );
      } else {
        $row_data[] = $bid;
      }
      $row_data[] = $b->bundle();
      $row_data[] = $b->label();
      $block_rows[] = $row_data;
    }

    $page[] = [
      '#title' => t('Blocks Resuls'),
      '#markup' => sprintf($entity_discovery_message, 'blocks', count($blocks)),
    ];
    $page[] = [
      '#title' => t('Affected Blocks'),
      '#type' => 'table',
      '#header' => [
        'col1' => t('ID'),
        'col2' => t('Block Type'),
        'col3' => t('Label'),
      ],
      '#rows' => $block_rows,
    ];

    return $page;
  }
}
