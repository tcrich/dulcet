#!/bin/bash

# Constants
RESET='\033[0m'
RED='\033[38;5;1m'
GREEN='\033[38;5;2m'
YELLOW='\033[38;5;3m'
MAGENTA='\033[38;5;5m'
CYAN='\033[38;5;6m'

echo_pass() {
  echo -e "${CYAN}$1: ${GREEN}PASS${RESET}"
}

echo_fail() {
  echo -e "${CYAN}$1: ${RED}FAIL${RESET}"
}
