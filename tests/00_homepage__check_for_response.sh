#!/bin/bash

source bin/logs-colors.sh

port=${1:-8081}
test_label="[$(basename $0)] Homepage responds with 200"
STATUS=$(curl -sI http://localhost:$port | awk '/HTTP\/1.1/ { print $2 }')

if [[ "${STATUS}" != 200 ]]; then
  echo_fail "$test_label"
  exit 1
fi

echo_pass "$test_label"
