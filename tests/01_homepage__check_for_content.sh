#!/bin/bash

source bin/logs-colors.sh

# default to the test port (8081)
port=${1:-8081}
URL="http://localhost:$port"

echo "[$(basename $0)] Homepage Tests"
RESPONSE=$(curl -sS $URL)

test_label="String 'Duke University Libraries' exists in home page source..."
if [[ ! "$RESPONSE" =~ "Duke University Libraries" ]]; then
  echo_fail "$test_label"
  exit 1
fi
echo_pass "$test_label"

# exit 0 for a successful test
exit 0

