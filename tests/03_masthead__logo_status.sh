#!/bin/bash

source bin/logs-colors.sh

port=${1:-8081}

echo "[$(basename $0)]: Masthead Logo"
URL="http://localhost:$port/masthead/img/logo.png"
test_label="Accessing Masthead Logo produces '200 OK' status"

STATUS=$(curl -sI $URL | awk '/HTTP\/1.1/ { print $2 }')

if [[ "${STATUS}" != 200 ]]; then
  echo_fail "$test_label"
  exit 1
fi

echo_pass "$test_label"

# exit 0 for a successful test
exit 0
