#!/bin/bash

source bin/logs-colors.sh

port=${1:-8081}

echo "[$(basename $0)]: Footer Logo"
URL="http://localhost:$port/themes/custom/drupal9_dulcet/images/footer/dukelogo_vert_60pct_140.png"
test_label="Accessing Footer Logo produces '200 OK' status"

STATUS=$(curl -sI $URL | awk '/HTTP\/1.1/ { print $2 }')

if [[ "${STATUS}" != 200 ]]; then
  echo_fail "$test_label"
  exit 1
fi

echo_pass "$test_label"
