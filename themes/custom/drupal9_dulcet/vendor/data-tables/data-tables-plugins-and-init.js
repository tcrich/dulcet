// ## vsorting plugin via cdn.datatables.net/plug-ins/1.11.3/sorting/num-html.js
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
  "num-html-pre": function ( a ) {
      var x = String(a).replace( /<[\s\S]*?>/g, "" );
      return parseFloat( x );
  },

  "num-html-asc": function ( a, b ) {
      return ((a < b) ? -1 : ((a > b) ? 1 : 0));
  },

  "num-html-desc": function ( a, b ) {
      return ((a < b) ? 1 : ((a > b) ? -1 : 0));
  }
} );

// ## init data tables for adopt digital collection
(function ($) {

  // need to add IDs
  $('#main-content .adopt-datatable .table-responsive .table').attr('id', 'adopt-datatable');
  $('#main-content .adopt-datatable.filter-by-group').attr('id', 'adopt-datatable-filter');
  
  // clean up filter list and add wrapping markup
  $sorting_group_list = [];
  $('#adopt-datatable-filter div').each(function(i) {
    $sorting_group_list[i] = $(this).text();
  });

  $filter_content = '<div class="filters"><p>Filter by Collection Group</p><form><select id="adopt-datatable-filter" aria-label="Select Group"><option value="">-- select a group --</option>';

  $.each(unique($sorting_group_list), function(i, value){
    if (value !== '') {
      $filter_content += '<option>' + value + '</option>';
    }
  });

  $filter_content += '</select></form></div>';

  $('#adopt-datatable-filter').replaceWith($filter_content);
  
  function unique(array) {
    return $.grep(array, function(el, index) {
      return index === $.inArray(el, array);
    });
  }

  // convert adoption amounts to USD format
  $('#adopt-datatable .money').each(function() {
    var amount = $(this).text();
    var newAmount = '$' + amount.replace(/\B(?=(\d{3})+(?!\d))/g, ",");  
    $(this).text(newAmount);
  });

  // init for target table
  $('#adopt-datatable').DataTable( {
      "fixedHeader": true,
      "paging":   false,
      "iDisplayLength": 5,
      "lengthMenu": [ 5, 10, 20, 50, 100 ],
      "info":     false,
      "responsive": true,
      columnDefs: [
        { type: 'num-html', targets: 3 },
        { "targets": [ 1, 4 ], "visible": false, "searchable": true},
      ]
  } );

  // enable group filtering
  var oTable;
  oTable = $('#adopt-datatable').dataTable();

  $('#adopt-datatable-filter').change( function() {
      oTable.fnFilter( $(this).val());
  });

}) (jQuery);
