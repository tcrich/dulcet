/* Additional tabs behavior */

jQuery(document).ready(function ($) {
  /* Make sticky tabs provide bookmarkable URLs */
  $('.nav-tabs-sticky').stickyTabs();
});
