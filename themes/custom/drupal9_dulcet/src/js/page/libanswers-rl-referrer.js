/* Auto-populate the "Site URL / Related Link" field upon */
/* LibAnswers form interaction, provided a value in a     */
/* "referrer" parameter in the URL. */

jQuery(document).ready(function ($) {
  var urlHasChanged = false;

  /* Capture URL parameters */
  $.urlParam = function(name){
      var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
      if (results==null){
         return null;
      }
      else{
         return results[1] || 0;
      }
  }

  referrer = '';

  if($.urlParam('referrer') !== null) {
    referrer = decodeURIComponent($.urlParam('referrer'));
  }

  /* First time a user clicks on a text input field in the Ask RL form, */
  /* populate the field. Note dependency on widget & field ids.         */

  $('#s-la-widget-14799').on('focus', 'input', function() {
    if((urlHasChanged === false)) {
      $('input#custom2_14799').val(referrer);
      urlHasChanged = true;
    }
  });

});
