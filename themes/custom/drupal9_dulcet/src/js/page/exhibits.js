/* Uses JS to tweak Exhibit pages */

jQuery(document).ready(function ($) {
  
  // remove border from H1
  $('body.page-node-type-exhibits-page h1').addClass('no-border');
    
  // Adds empty alt text to images on exhibit pages
  $('#main-content .exhibits-page__field-image img').each(function () {
    if ($(this).attr('alt') === undefined) {
      $(this).attr('alt', '');
    }
  });

});
