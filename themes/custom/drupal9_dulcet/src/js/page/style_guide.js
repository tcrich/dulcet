/* ========================================================= */
/* Scripts used by the Style Guide page.                     */
/* ========================================================= */

/* Get hex color from RGB. css('background-color') returns e.g. rgb(4, 52, 130) */
/* See https://stackoverflow.com/a/1740716 */
function rgb2hex(rgb) {
  if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;

  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
  function hex(x) {
    return ("0" + parseInt(x).toString(16)).slice(-2);
  }
  return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

jQuery(document).ready(function ($) {

  $('#style-guide-color-swatch').each(function(){
    var duke_colors = [
      'duke-navy-blue',
      'duke-royal-blue',
      'copper',
      'persimmon',
      'dandelion',
      'piedmont',
      'eno',
      'magnolia',
      'prussian-blue',
      'shale-blue',
      'ironweed',
      'hatteras',
      'whisper-gray',
      'ginger-beer',
      'dogwood',
      'shackleford',
      'cast-iron',
      'graphite',
      'granite',
      'limestone'
    ];

    $('#color-swatch-duke-brand').html('');

    jQuery.each(duke_colors, function(i, color) {
      $('#color-swatch-duke-brand').append('<div class="col-sm-2 mb-3"><div class="rounded-circle color-swatch swatch-'+color+'"></div><code>$'+color+'</code></div>');
    });

    var dul_colors = [
      'primary',
      'secondary',
      'success',
      'info',
      'warning',
      'danger',
      'dul-masthead-blue',
      'dul-dulcet-lightblue',
      'light',
      'dark',
      'link-color',
      'link-hover-color'
    ];

    $('#color-swatch-dul-brand').html('');

    jQuery.each(dul_colors, function(i, color) {
      $('#color-swatch-dul-brand').append('<div class="col-sm-2 mb-3"><div class="rounded-circle color-swatch swatch-'+color+'"></div><code>$'+color+'</code></div>');
    });

    $('.color-swatch').each(function(){
      $(this).text(rgb2hex($(this).css('background-color')));
    });

    jQuery.each([0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100], function(i, pct) {
      $('#color-swatch-tint').append('<div class="col-sm-1 mb-3"><div class="tint-swatch dul-masthead-blue-tint-'+pct+'"></div><code>'+pct+'%</code></div>');
      $('#color-swatch-shade').append('<div class="col-sm-1 mb-3"><div class="tint-swatch dul-masthead-blue-shade-'+pct+'"></div><code>'+pct+'%</code></div>');
    });

  });

  /* Report font styles for body copy */

  $('#styleguide-body-font').text($('body').css('font-family'));
  $('#styleguide-body-size').text($('body').css('font-size'));
  $('#styleguide-body-color').text(rgb2hex($('body').css('color')));
  $('#styleguide-body-lineheight').text($('body').css('line-height'));

});