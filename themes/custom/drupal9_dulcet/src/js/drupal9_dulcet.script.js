import 'popper.js';
import 'bootstrap';
import 'jquery-stickytabs';
import '@fortawesome/fontawesome-free/js/all';
import './new_additions.js';
import './daily-hours-display.js';
import './tabs.js'
import './tooltips.js'

(function () {

  'use strict';

  Drupal.behaviors.helloWorld = {
    attach: function (context) {
      // console.log('Hello World');
    }
  };

})(jQuery, Drupal);
