# DUL Drupal 9 - Adding Modules

In a simple, non-CI/CD environment, adding modules to a Drupal 9 instance is as simple as typing `$ composer require drupal/my-module-here` on the command line (from a hosting server).

However, in the semi-rigid and formal environment for our Drupal 9 instance, the process of adding a module involves:

- testing a module on a ephemeral sandbox environment,

- adding RUN `composer require drupal/my-module-here[:~version-tag]` to our project’s `.docker/Dockerfile`, and finally,

- adding the module to `config/core.extension.yml`, and finally,

- committing the changes to our GitLab repository and merging with the develop branch.

Doing these steps will ensure that a module will be installed on our `staging` and then `production` environments during a CI/CD deployment job.


## Testing Module(s) on Sandbox

Feel free to SSH into the sandbox server and add a module via `composer`

```
$ ssh dulcet-sandbox.lib.duke.edu
$ docker exec -it drupal9_app /bin/bash

I have no name!@4ded958d14a8:/opt/bitnami/drupal$ composer require drupal/modname-here
I have no name!@4ded958d14a8:/opt/bitnami/drupal$ drush en modname-here
```

At this point, your module should be installed and enabled on our “sandbox” server.

### Additional Modules

Some modules include additional modules that may or may not be enabled. Please use drush or the GUI to enable and test these additional modules.

## Adding Modules to Dockerfile

After you have verified the usefulness of the module(s) as well as verifying the module doesn’t break anything in our setup – add this line to our `.docker/Dockerfile`:

```
# // shortened for brevity
# ...
# look for other 'composer' statements, then:

RUN composer require drupal/modname-here[:~version-tag]
```

## Adding Modules to `config/core.extension.yml`

Open `config/core.extension.yml` and add the new module:

```
_core:
  default_config_hash: R4IF-ClDHXxblLcG0L7MgsLvfBIMAvi_skumNFQwkDc
module:
  // shortened for brevity
  modname-here: 0
```

## Commit Changes to GitLab

Once you have made the necessary changes, commit your work to our project’s GitLab repository, and then create a merge request.
