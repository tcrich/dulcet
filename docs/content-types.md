# Available D9 Content Types
## General Pages
These all have the same fields & functionality. The types are used to help content authors filter lists of pages in the admin UI.
* page ("Basic Page" = Perkins pages)
* rubenstein_page (Rubenstein pages)
* datavis_page (Datavis/CDVS pages)
* lilly_page (Lilly Library pages)
* music_page (Music Library pages)

## Specialized Pages
These include specific fields and have custom templates.
* exhibit_page (Exhibits pages)
* job_posting (Job Posting pages)
