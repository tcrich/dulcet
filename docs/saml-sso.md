# SAML/Single-Sign-On (SSO) Setup

Third-party module, `samlauth`, is installed and will be used to faciliate SSO. The module will also create 
Drupal accounts with data provided from Duke's IdP.
### Configuration Settings
As the "admin" user, visit the `/admin/config/people/saml` route, or select ...  

#### Service Provider section
Enter your SP entity id for **Entity ID** (e.g. https://mylibhost.lib.duke.edu)  
  
Enter `/sp/sp.key` for **Private Key Filename**. This file will be present in the container.  
  
Enter `/sp/sp.pem` for **X.509 Certificate Filename**. This file will (also) be present in the container.

#### Identity Provider section

Many SP frameworks have the ability to parse an XML to retrieve the Identity Provider information. However, the `samlauth` module does not have this feature.  
  
This information will need to be copied/pasted from the `duke-metadata-2-signed.xml` provided in this repo.

