# Apache Web Server Considerations

Create a file in your local development setup, and call it `my_vhost.conf`. Then, copy the contents below into the file:
```httpd
# CUSTOMIZED DUL flavor...
# 
# Not exactly sure you need this
# ServerName your-local-hostname-here.lib.duke.edu:8080

<VirtualHost 127.0.0.1:8080 _default_:8080>
  ServerAlias your-local-hostname-here.lib.duke.edu
  DocumentRoot /opt/bitnami/drupal
  LogLevel debug
  <Directory "/opt/bitnami/drupal">
    Options -Indexes +FollowSymLinks -MultiViews
    AllowOverride None
    Require all granted
  </Directory>

  # this block will ensure the masthead is available
  Alias "/masthead" "/opt/dul/masthead"
  <Directory "/opt/dul/masthead">
    AllowOverride none
    Options FollowSymLinks IncludesNoExec
    AddOutputFilter INCLUDES html
    Require all granted

    LogLevel warn
  </Directory>

  # Don't remove this!
  Include "/opt/bitnami/apache/conf/vhosts/htaccess/drupal-htaccess.conf"
</VirtualHost>
```
  
After you save it, then adjust your `docker-compose.yml` to expose (or mount) this file inside the Drupal container:
```yml
version: '3.7'

volumes:
  drupal_data:
    driver: local

services:
  mariadb:
    # configuration removed for brevity

  drupal9:
    # Set this to 'bridge' or comment the line out
    # when using a database container
    # network_mode: host

    stdin_open: true
    tty: true
    container_name: drupal9_app
    build:
      dockerfile: Dockerfile
      context: .
    ports:
      - '80:8080'
      - '443:8443'
    environment:
      # removed for brevity
    volumes:
      - 'drupal_data:/bitnami/drupal'
      - './config:/opt/dul/config:rw'
      - './my_vhost.conf:/vhosts/drupal-vhost.conf:ro'
```
  
After this, verify that the container starts correctly.

