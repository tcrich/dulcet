# Deployment for Dulcet

[[_TOC_]]

## Copying Assets from Drupal 7 Instance

SSH into one of the `cms-*` VMs to create archives of the content under Drupal:  
  
```bash
$ ssh cms

$ cd /var/www/drupal7/sites/default/files
```
  
Next, use the `tar` command to create the archives:  
  
```bash
$ cd dul && sudo tar --exclude='./dul' -zcvf /nas/lib_assets_share/drupal9/archives/dul.tgz && cd .. 
$ cd rubenstein && sudo tar --exclude='./rubenstein' -zcvf /nas/lib_assets_share/drupal9/archives/rubenstein.tgz && cd .. 

# note that we're referring to the "data" site as:
# 'datagis' in the Drupal 7 ecosystem, and
# 'datavis' in the Drupal 9 ecosystem.
$ cd datagis && sudo tar --exclude='./datagis' -zcvf /nas/lib_assets_share/drupal9/archives/datavis.tgz && cd .. 

# Lilly and Music Library assets are under the 'east' site.
$ cd east && sudo tar --exclude='./east' -zcvf /nas/lib_assets_share/drupal9/files/east.tgz . && cd ..

# settings permissions (optional)
$ sudo chown webadmin:sysadmins /nas/lib_assets_share/drupal9/archives/*.tgz
```
  
NOTES:
* We're using the `--exclude` option omit the duplicate named folder (e.g. `/var/www/drupal7/sites/default/files/dul/dul`)
* Network share folder, `oit-nas-fe11.oit.duke.edu:/LIB-ASSETS-SHARE` is mounted at `/nas/lib-assets-share` on all `cms-*` VMs.
* You'll need to mount the same volume on your personal workstation if local access is needed.
  
Assuming access to these files, then proceed to the next stage...  
  
## Create `default-site` Directory

In your development environment, or where the application will reside on production (and/or staging):  
  
```bash
$ cd /path/to/dulcet
$ mkdir -p default-site/files && cd default-site/files
$ mkdir dul rubenstein datavis east

$ cp /my/mount/path/to/lib_assets_share/drupal9/archives/dul.tgz default-site/files/dul/
$ cp /my/mount/path/to/lib_assets_share/drupal9/archives/rubenstein default-site/files/rubenstein/
$ cp /my/mount/path/to/lib_assets_share/drupal9/archives/datavis.tgz default-site/files/datavis/
$ cp /my/mount/path/to/lib_assets_share/drupal9/archives/east.tgz default-site/files/east/
```

## Adjust `docker-compose.yml` / Add volume
## `docker-compose.yml` Deployment
The correct `docker-compose.yml` file will be copied to the server via DevOps' `ansible-playbook` process.  
  
### ENVIRONMENT VARIABLES
The following variables are set, as needed:
* **`DRUPAL_SKIP_BOOTSTRAP`**
* **`DRUPAL_CONFIG_SYNC`** (set when `DRUPAL_SKIP_BOOTSTRAP=yes`)
* **`ENABLE_SAML`**
* **`SP_ENTITY_ID`**
* **`GOOGLE_ANALYTICS_ID`**  
  
### Volumes
Various volumes are mounted, particularly:  
* CDVS dataset files are mounted,
* The `config` directory is mounted to `/opt/dul/config`,
* "Entrypoint" scripts are mounted
  

Under the `drupal9:volumes` section, add an entry for your newly created `default-site`:  
  
```yml
services:
  mariadb:
    ...

  drupal9:
    ...
    volumes:
      - './default-site:/opt/dul/default-site:rw'
      ...
```
  
Now you're ready to bootstrap the application.  
  
## Entrypoint Script / `06-copy-assets.sh`
The `06-copy-assets.sh` "entrypoint" script is responsible for detecting the presence of the `/opt/dul/default-files` directory.  
  
When present, the script will extract the `*.tgz` files if present.
