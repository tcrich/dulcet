# Drupal 9 Architecture for DUL Website (Dulcet)

Throughout this documentation, "dulcet" will refer to the application stack.

[[_TOC_]]

## Docker, Dockerfiles, and Docker-Compose

The root directory for the "dulcet" project is located at:  
`/opt/docker-compose/projects/dulcet`  

### Docker
The Drupal 9 instance, as well as the accompanying MariaDB service, run from Docker containers.

### Dockerfile
Our `Dockerfile` extends the [Bitnami Drupal Docker](https://github.com/bitnami/bitnami-docker-drupal) image
  
`.docker/Dockerfile` is the canonical file for production use.  
`Dockerfile.sample` is referred to by developers  

Then we:
* use `composer` to add contributed modules,
* install `nodejs` and `yarn`, used to compile theme assets,
* set several environment variables (some are deprecated now)
  
### Docker-Compose
We use `docker-compose` to manage the application stack.  
  
We don't manage any `docker-compose.yml` in this repository. This file is deployed from the DevOps Ansible Playbook.

## Continuous Integration/Deployment `(.gitlab-ci.yml)`

Our CI/CD procedure is still a fluid work-in-process. However, DevOps has settled on a deployment strategy that only uses the `deploy` stage.

## Logging

### Web Access
The logs generated from the `staging` and `production` Drupal containers can be accessed on the [Event Logging](https://event-logging.lib.duke.edu) dashboard.

### VM Access
The logs generated from the Drupal containers can be viewed by:  
```sh
$ ssh dulcet(-staging).lib.duke.edu
$ sudo tail -f -n1000 /var/log/dulcet.log
```

## Ansible Playbook / Inventory
**Playbook Name:** `drupal9.yml`  
  
The DevOps' [antsy](https://gitlab.oit.duke.edu/devops/antsy) playbook suite manages the state of the VMs used to host our Drupal 9 instances.

The Drupal 9 VMs belongs to the `drupal9` inventory group.  

| Hostname | Application Hostname | Inventory | OS  |
| ---      | ---                  | ---       | --- |
| dulcet.lib.duke.edu | library.duke.edu | production | debian |
| dulcet-staging.lib.duke.edu | pre2.library.duke.edu | staging | debian |
| dulcet-sandbox.lib.duke.edu | - | development | centos |

### Docker-Compose File
The `docker-compose.yml` file for each VM is deployed from the `drupal9.yml` playbook.

### Convenience Bash Scripts
These are 'convenience' scripts deployed from the `drupal9.yml` playbook.  
(each script is copied to `/usr/local/bin`):  
  
* `reload-dulcet`
* `restart-apache`
* `export-config`
  
## Cron Jobs

### Database Backup

### Docker System Prune

