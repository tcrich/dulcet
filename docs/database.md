# Database Backup/Restoration

## Automated Backup Process

Backups of the Drupal 9 database are generated daily (via system `cron` job) and stored under `/var/lib/dulcet/backups`.
  
Note: This directory is mounted into the Drupal 9 container as such:
```yml
services:
  db:
    ...

  app:
    volumes:
      - '/var/lib/dulcet/backups:/backups:rw'
```

## Manual Backup Process
Creating a manual is easy and straightforward.  
  
First, connect to the "dulcet" VM:
```sh
$ ssh dulcet-staging # or dulcet-prod (see DevOps for correct host names)
```
  
Then, run this command against the `drupal9_app` container:  
```sh
$ docker exec -it drupal9_app drush sql:dump --result-file=/backups/drupal-$(date +%Y%m%d-%H.%M.%S).sql
```
  
A file named `/var/lib/dulcet/backups/drupal-YYYYMMDD-HH.mm.ss.sql` will be created -- remember this file name.  

  
## Restoring Drupal Data

### Docker-related

Navigate into the project root...  
```sh
$ cd /opt/docker-compose/projects/dulcet
$ docker-compose down
```
  

Bitnami Drupal bootstrap process creates a hidden file to indicate a successful bootstraping.  
Make sure to remove it when clearing out the Drupal perisistence.  
  
Then be sure to handle the rest of the persistence data.
```sh
$ sudo rm -rf /var/lib/dulcet/drupal/.user_scripts_initialized
$ sudo rm -rf /var/lib/dulcet/drupal/*
```
  
Clear the mariadb persistence data:
```sh
$ sudo rm -rf /var/lib/dulcet/mariadb/*
```
  
Copy the file above to the 'dulcet' root:  
```sh
$ cp /var/lib/dulcet/backups/drupal-YYYYMMDD-HH.mm.ss.sql drupal9_2.sql
```

SIDEBAR NOTE: `drupal9_2.sql` is mounted into the "mariadb" container as such:
```yml
services:
  mariadb:
    image: docker.io/bitnami/mariadb:10.5
    container_name: drupal9_db
  volumes:
    - './drupal9_2.sql:/docker-entrypoint-initdb.d/drupal9_2.sql'
```
  
Finally, restart the containers.  
  
```sh
$ docker-compose up -d
```
  
When the `mariadb` container (named: 'drupal9_db') boostraps, it will import the `drupal9_2.sql` data file to import 
the data.
