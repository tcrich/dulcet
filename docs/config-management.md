# Configuration Management
Drupal 8/9 encodes all site configuration as `.yml` files (see [/config](https://gitlab.oit.duke.edu/dul-its/dulcet/-/tree/main/config) directory in our `dulcet` repo). This ensures that config changes have a clear commit history alongside code changes. It also simplifies sharing config between different developers and environments.

The `config` directory is mounted into the container (instead of being copied into it during Docker build process).

During a build, all [config gets imported](https://gitlab.oit.duke.edu/dul-its/dulcet/-/blob/main/docker-entrypoint-init.d/10-import-config.sh) from the [/config](https://gitlab.oit.duke.edu/dul-its/dulcet/-/tree/main/config) directory.

Almost anything you change or add in the Drupal UI that isn't _content_ will generate some changes to config that need to get shared back to the code repo if you want them to persist. E.g., revising block placement, tweaking a View, changing a setting on a module, etc.


## Have I Made Config Changes?
In the Drupal admin UI, visit `Configuration` > `Configuration synchronization` (http://localhost/admin/config/development/configuration). If you have made no changes to config since building, you'll see:
> There are no configuration changes to import.

If you change something that touches config (e.g., place an alert block in the header region) and revisit the [configuration page](http://localhost/admin/config/development/configuration) you will see:
> The following items in your active configuration have changes since the last import that may be lost on the next import. `block.block.alert_aleph_maintenance`

Click the `View differences` button near any file to see what has changed.
The new config files your changes created show up under a `Removed` header (counterintuitively), because if you were to click the `Import all` button they would be removed. They are in the active config but they are not in the canonical "sync directory" ([/config](https://gitlab.oit.duke.edu/dul-its/dulcet/-/tree/main/config)).


## Config Management Plan

Based on advice from peers, let’s follow these steps when syncing config:

### **1. Use the admin GUI to view current config state:**

https://library.duke.edu/admin/config/development/configuration

If there are active changes to the config, we’ll need to get those synced before deploying any changes to dulcet that also have config changes.

It’s probably a good practice to copy the list of active config files to have for comparison later on.

### **2. Export the config:**

Open a new terminal, and run the following:

```
ssh dulcet.lib.duke.edu
docker exec drupal9_app drush config-export
```
**OR**

use the sync tool in the GUI:
https://library.duke.edu/admin/config/development/configuration/full/update
(select ‘sync’ as config source, click ‘update configuration’ button)

Either of these steps will export the active configuration as .yml files to:
`/opt/docker-compose/projects/dulcet/config`

If you want to export to a backup directory, you can:

```
docker exec drupal9_app drush config-export --destination=/backups/config
```

### **3. Make a fresh local branch off of latest ‘pulled’ Develop**

- I’d typically name it something like `config-export-20220509`
- The config files will be copied directly into the project folder so we can use Git to compare changes

### **4. Copy (rsync) the config files from the server to your machine**

In a new tab, run the following to pull down files from the server to your machine:

```
rsync -av --delete [your user name]@dulcet.lib.duke.edu:/opt/docker-compose/projects/dulcet/config /local/path/to/root/of/dulcet/theme/
```

Note:
- the `--delete` flag ensures that if a config file was deleted/removed from prod, it will get removed from your local `config` directory
- you can also add the `--dry-run` flag to test that rsync is going to do what you want it to do

### **5. Commit changes**
- Do a `git status` to see which configs have changed
- compare that to the list you copied in step 1
- if all looks well, do a `git add` for the changed files and commit them to your branch, push them up, and do an MR when you're ready


## Troubleshooting

If things seem out of sync, try running:

```
docker exec drupal9_app drush config-import --diff
```

**Notes from Derrek:**
When specifying `--destination=/backups/config`, the host directory is actually `/var/lib/dulcet/backups/config`

The docker-compose file is our friend (helpful for finding paths, etc):
https://gitlab.oit.duke.edu/dul-its/dulcet/-/blob/develop/docker-compose.sample.yml#L81


## More Config Management Resources
- [Drupal 9 Configuration Management Docs](https://www.drupal.org/docs/configuration-management)
- [Config Direct Save Module](https://www.drupal.org/project/config_direct_save).
  This is what powers the tabbed UI showing changes and enables the `Update` tab with the easy-to-sync `Update configuration` button.
