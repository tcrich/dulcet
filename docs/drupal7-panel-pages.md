# Panel Pages

**Panel Pages** are represented by the `page_manager` and `panels` modules in a Drupal 7 instance.  
  
To date, there are 156 "panel pages" in the DUL site. This can be examined by the following SQL query:
```sql
MariaDB [perkins]> select count(*) from page_manager_pages;

+----------+
| count(*) |
+----------+
|      156 |
+----------+
1 row in set (0.00 sec)

```
To begin inspecting the Libraries' `/home` page, we can discover its row by:
```sql
MariaDB [perkins]> select name, task, admin_title, admin_description, path from page_manager_pages where path = 'home';
+---------------+------+---------------+-------------------+------+
| name          | task | admin_title   | admin_description | path |
+---------------+------+---------------+-------------------+------+
| dul_home_page | page | DUL home page |                   | home |
+---------------+------+---------------+-------------------+------+
1 row in set (0.00 sec)
```

## Handlers
Page Manager Handlers are the next level in the data hierarchy for a "Panel Page".  
  
Each handler includes a `task` and `subtask`. The `page manager` module represents a page handler by setting its `task` value to "page", and its `subtask` value to the top-level 'name' of the page.  
  
In this case, 'dul_home_page'.  
  
Additionally, the `page_manager` module creates a conceptual "context" for this handler and stores its value as the handler's 
`name` field.  
  
Let's continue to use our home page to inspect further:
```sql
MariaDB [perkins]> select name, task, subtask, handler, weight from page_manager_handlers where 
    -> where task = 'page' and subtask = 'dul_home_page';
+----------------------------------+------+---------------+---------------+--------+
| name                             | task | subtask       | handler       | weight |
+----------------------------------+------+---------------+---------------+--------+
| page_dul_home_page_panel_context | page | dul_home_page | panel_context |      0 |
+----------------------------------+------+---------------+---------------+--------+
1 row in set (0.00 sec)
```

## Panel Display

Now that we have a top-level page and it's associated handler (panel_context), **page_manager** creates a 
"Display" structure (or row) whose 'machine_name' has the following naming convention:  
  
`page_<machine_name>_<handler_context>`, where  
  
* `<machine_name>` is the value generated for you when creating the page,
* `<handler_context>` is one of two values -- "panel_context" or "http_response"
  
For now, we'll be focused on the "panel_context" variant.

Let's see the "Panel Display" for the Libraries' home page:

```sql
MariaDB [perkins]> select * from panels_display where storage_id = 'page_dul_home_page_panel_context';
+-----+-----------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------+------------+------------+--------------------------------------+--------------+----------------------------------+
| did | layout    | layout_settings | panel_settings                                                                                                                                                                      | cache  | title | hide_title | title_pane | uuid                                 | storage_type | storage_id                       |
+-----+-----------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------+------------+------------+--------------------------------------+--------------+----------------------------------+
| 105 | radix_dul | a:0:{}          | a:1:{s:14:"style_settings";a:8:{s:7:"default";N;s:6:"center";N;s:4:"left";N;s:5:"right";N;s:7:"sidebar";N;s:13:"contentheader";N;s:14:"contentcolumn1";N;s:14:"contentcolumn2";N;}} | a:0:{} |       |          1 |          0 | 7d1911b8-acf6-49cf-92c0-8a1d50697551 | page_manager | page_dul_home_page_panel_context |
+-----+-----------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------+------------+------------+--------------------------------------+--------------+----------------------------------+
1 row in set (0.000 sec)
```

We're still not at the place of getting "data" to export out of our Drupal 7 site, but we're close now!  
  
Pay attention to the "did" (or 'display id') column -- it will be very useful in the next step.

## Panel Pane
  
Panel Displays have a one-to-many relationship to Panel Panes.  
Panel Panes is where we'll find the HTML content we'll export. Sweet!

Let's look at the `panels_pane` table:
```sql
MariaDB [perkins]> desc panels_pane;
+---------------+--------------+------+-----+---------+----------------+
| Field         | Type         | Null | Key | Default | Extra          |
+---------------+--------------+------+-----+---------+----------------+
| pid           | int(11)      | NO   | PRI | NULL    | auto_increment |
| did           | int(11)      | NO   | MUL | 0       |                |
| panel         | varchar(255) | YES  |     |         |                |
| type          | varchar(255) | YES  |     |         |                |
| subtype       | varchar(255) | YES  |     |         |                |
| shown         | tinyint(4)   | YES  |     | 1       |                |
| access        | longtext     | YES  |     | NULL    |                |
| configuration | longtext     | YES  |     | NULL    |                |
| cache         | longtext     | YES  |     | NULL    |                |
| style         | longtext     | YES  |     | NULL    |                |
| css           | longtext     | YES  |     | NULL    |                |
| extras        | longtext     | YES  |     | NULL    |                |
| position      | smallint(6)  | YES  |     | 0       |                |
| locks         | longtext     | YES  |     | NULL    |                |
| uuid          | char(36)     | YES  |     | NULL    |                |
+---------------+--------------+------+-----+---------+----------------+
15 rows in set (0.001 sec)
```

From this list, we're interested in:
* `type`: 'custom', 'block', 'views', etc
* `subtype`: important when `type = 'block'`. This value will be `block-BID`, where `BID` is the block id (see below)
* `shown`: visible or hidden
* `uuid`: because everything in Drupal is tied to a UUID now
* `configuration`: when `type='custom'`, the body value and format will reside here

### Block types
When `type='block'` and `subtype='block-XX'` (where XX = bid), use this query to retrive the block's content:
```sql
MariaDB [perkins]> SELECT * FROM block_custom WHERE bid = XX
```

