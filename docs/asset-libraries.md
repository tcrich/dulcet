# Asset Libraries (CSS & JS)
## Global Scripts & Styles
The theme's [drupal9_dulcet.info.yml](https://gitlab.oit.duke.edu/dul-its/dulcet/-/blob/develop/themes/custom/drupal9_dulcet/drupal9_dulcet.info.yml) file defines the asset libraries that should apply to all pages throughout the site. E.g.,:
```yml
libraries:
  - radix/style
  - drupal9_dulcet/global-styles
  - drupal9_dulcet/global-scripts
  - flexslider/flexslider
  - flexslider/integration
```

Libraries are namespaced. Those prefixed by `drupal9_dulcet/` are local to our theme and can be seen in [drupal9_dulcet.libraries.yml](https://gitlab.oit.duke.edu/dul-its/dulcet/-/blob/develop/themes/custom/drupal9_dulcet/drupal9_dulcet.libraries.yml). Both `radix` & `flexslider` libraries come from their respective Drupal modules; see [radix.libraries.yml](https://git.drupalcode.org/project/radix/-/blob/8.x-4.x/radix.libraries.yml) and [flexslider.libraries.yml](https://git.drupalcode.org/project/flexslider/-/blob/8.x-2.x/flexslider.libraries.yml).

## Page-Specific Scripts & Styles
Asset libraries defined by our theme are specified in [drupal9_dulcet.libraries.yml](https://gitlab.oit.duke.edu/dul-its/dulcet/-/blob/develop/themes/custom/drupal9_dulcet/drupal9_dulcet.libraries.yml).

Scripts may be from external sources (e.g., `https://rubenstein.libanswers.com/1.0/widgets/14799`) or local to our theme ( `src/js/page/libanswers-rl-referrer.js`). They can also come from NPM packages (_see below_).

It's common to attach asset libraries only to the specific pages or sections of the site that need them (especially for performance). These rules are specified in the [drupal9_dulcet.theme](https://gitlab.oit.duke.edu/dul-its/dulcet/-/blob/develop/themes/custom/drupal9_dulcet/drupal9_dulcet.theme) file, e.g.:

```php
if ($alias == '/about/style-guide') {
      $page['#attached']['library'][] = 'drupal9_dulcet/style-guide';
    }
```

## Asset Injector
We occasionally use the [Drupal Asset Injector module](https://www.drupal.org/project/asset_injector) to add scripts to pages without defining libraries.

Scripts can be written and configured (e.g, which pages should render them) via `/admin/config/development/asset-injector`. Asset injector scripts are saved as configuration in `config/*.yml` files.

## Getting JS (or CSS) from an NPM Package

### Dulcet Theme Asset Libraries
When we define our theme's asset libraries in [drupal9_dulcet.libraries.yml](https://gitlab.oit.duke.edu/dul-its/dulcet/-/blob/develop/themes/custom/drupal9_dulcet/drupal9_dulcet.libraries.yml) we can use JS/CSS from NPM packages. This can make auditing and updating these dependencies easier, and also spares us from storing copies of other applications' codebases in our theme's code repository.

First, define the NPM package in [package.json](https://gitlab.oit.duke.edu/dul-its/dulcet/-/blob/develop/themes/custom/drupal9_dulcet/package.json):

```json
  "dependencies": {
    "vue": "^2.6.14"
  }
```

Then in `drupal9_dulcet.libraries.yml` use the path to the desired file(s) from the package (in the `node_modules` directory). You can find the path by either browsing the code repo for the NPM package, or browsing the `/opt/bitnami/drupal/themes/custom/drupal9_dulcet/node_modules` directory in the app container after a build.

```yml
js:
    node_modules/vue/dist/vue.min.js: { minified: true }
```


### Module Library Overrides
Some Drupal **modules** depend on additional third-party Javascript/CSS libraries that they do not include. By default, the common convention is for these modules to look for these dependencies in the `/libraries` directory and for an implementer to manually download these dependencies and place them there. But because we use NPM & Webpack, we can get these packages as part our front-end build process.

To do this, we define the NPM package in [package.json](https://gitlab.oit.duke.edu/dul-its/dulcet/-/blob/develop/themes/custom/drupal9_dulcet/package.json):

```json
  "dependencies": {
    "flexslider": "^2.7.2"
  }
```

Then override the library location to look in `./node_modules` in [drupal9_dulcet.info.yml](https://gitlab.oit.duke.edu/dul-its/dulcet/-/blob/develop/themes/custom/drupal9_dulcet/drupal9_dulcet.info.yml):
```yml
libraries-override:
  radix/bootstrap: false
  flexslider/flexslider:
    js:
      /libraries/flexslider/jquery.flexslider-min.js: ./node_modules/flexslider/jquery.flexslider-min.js
    css:
      component:
        /libraries/flexslider/flexslider.css: ./node_modules/flexslider/flexslider.css
```
See [libraries-override Drupal docs](https://www.drupal.org/docs/theming-drupal/adding-stylesheets-css-and-javascript-js-to-a-drupal-theme#override-extend) for more.

## More Resources
* [Drupal Docs for Adding CSS & JS to a Theme](https://www.drupal.org/docs/theming-drupal/adding-stylesheets-css-and-javascript-js-to-a-drupal-theme)

