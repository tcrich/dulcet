# Content Migration from Drupal 7

## Export Process
DevOps has created two separate export streams:  
### Nodes, Blocks and Panel Pages
Derrek Croney has [created a series of Perl scripts to quickly convert nodes](https://gitlab.oit.duke.edu/dlc32/drupal-data-export), blocks and panel pages into JSON-formatted objects -- stored in individual files.
  
With the Perl scripts and the Dulcet project present on the same VM/server, we can do this:
```bash
$ cd /path/to/drupal_data_export
$ ./node-export.pl -O ~/path/to/dulcet/drupal7-export/perkins perkins
```
### Asidebox Blocks, Feeds, Menus
The `dul_system` and `asidebox` modules have been extended (on our DUL staging VM) to include export functions.  The functions are available using the Drush program.  

```bash
$ ssh cms-pre

# navigate to the sites directory
$ cd /var/www/drupal7/sites

# export the feeds
$ cd perkins && drush6 dul-feeds > ~/feeds-perkins.json
$ cd rubenstein && drush6 dul-feeds > ~/feeds-rubenstein.json
$ cd datavis && drush6 dul-feeds > ~/feeds-datavis.json
$ cd east && drush6 dul-feeds > ~/feeds-east.json

# export the menus
$ cd perkins && drush6 dul-menus > ~/menus-perkins.json
$ cd rubenstein && drush6 dul-menus > ~/menus-rubenstein.json
$ cd datavis && drush6 dul-menus > ~/menus-datavis.json
$ cd east && drush6 dul-menus > ~/menus-east.json

# use rsync on your server to copy these files
```

### Exporting Module-based Blocks Used with Asidebox (Drupal7)
```sh
$ curl -o /path/to/dulcet/drupal7_export/[perkins|rubenstein|...]/asideblocks.json "https://pre2.library.duke.edu/asidebox/export_blocks"
```
