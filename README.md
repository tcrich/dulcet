# Dulcet

DUL's Drupal 9 Environment & Configuration

[[_TOC_]]

## Quick Notes

This project creates a "Bitnami/Drupal"-derived image both for local use and server use. The `Dockerfile` indicates the specific version of Drupal to use.

## Content & Migration

* [Content Types](./docs/content-types.md).
* [Migration Notes](./docs/drupal-migration.md).

## Local Use

### Getting Started / Environment Setup
* Copy `docker-compose.sample.yml` to `docker-compose.yml`
* Copy `Dockerfile.sample` to `Dockerfile`
* Adjust both files to your liking
* Copy sql data to local instance:
```sh
rsync -av dulcet-staging:/srv/mariadb-datastore/drupal9_staging_latest.sql drupal9_local.sql
```

#### Environment Variables
Below are some key environment variables that tweak the behavior
of the container's initialization; set in `docker-compose.yml`:
  
* `BITNAMI_DEBUG` - set to `yes` to see `DEBUG` level messages in the logs (helpful for troubleshooting)
* `ENABLE_SAML` - enable SP/SSO/Shibboleth integration. Defaults to `no`, and it likely unneeded in local dev setups.
* `IMPORT_ALL_DATA` - by default, only the "Perkins" data is imported. Set this to `yes` to import all available data from Rubenstein, Data, and East libraries.
* `BYPASS_IMPORT` - Set this to `yes` to skip all data import. Can be useful if working on something unrelated to content.
* `USE_DUL_FRONTPAGE` - Setting this to `yes` will make the `/dul_home_page` panel page the default "front page".
* `IMPORT_EXHIBITS` - Set this to "true" to copy exhibits images to `sites/default/files` inside the Drupal container.
* `BYPASS_CONFIG_IMPORT` - Set `yes` to bypass the Drupal config import process.
* `GOOGLE_ANALYTICS_ID` - If set, this value will be used by the Google Analytics module. Ignored when `BYPASS_CONFIG_IMPORT=yes`.
* `DRUPAL_SKIP_BOOTSTRAP` - While this is used by Bitnami, when set to `yes`, we'll bypass installing modules and themes needed for our instance.
  
### Run the container

Force the bootstrapping process:
```sh
$ [sudo] docker-compose up --build
```
in a new tab, run yarn to complie assets and then clear cache:
```sh
docker exec -u 0 -t drupal9_app /bin/bash -c "cd themes/custom/drupal9_dulcet ; yarn && yarn dev ; drush cr"
```
after build completes, the app should be available at `localhost`

### Drupal Admin User
Use the `/user` route to log in as the administrator user.
The credentials are `user/bitnami`.
  
If you are connecting to a non-containerized database instance, be sure to set `network_mode: host`.

### Clear Persistent Data
```sh
# the order matters...
$ docker container rm dulcet_mariadb_1 drupal9_app
$ docker volume rm dulcet_drupal_data dulcet_mariadb_data
```

You may occasionally have to prune (remove the build cache, containers, networks, & dangling images):

`$ docker system prune -af`

### Running Commands Against the Containers
Assuming the container name is `drupal9_app` and `dulcet_mariadb_1` (for the application and database):
```sh
# run drush command against app container
$ docker exec -it drupal9_app drush <your-command> [options]

# clear the cache (you'll have to do this often while developing):
$ docker exec -it drupal9_app drush cache:rebuild

# connect to the database
$ docker exec -it dulcet_mariadb_1 mysql -uroot -pcornball -Ddrupal9_2
```

### Shell into the app container
It may be useful to edit the theme files from directly within the app container for quicker iteration. Assuming the container name is `drupal9_app`:
```sh
$ docker exec -u 0 -it drupal9_app bash
$ cd /opt/bitnami/drupal/themes/custom/drupal9_dulcet
$ vi drupal9_dulcet.theme
(make edits)
$ drush cache:rebuild
(repeat until it's doing what you intend, then make those same edits to the code in your text editor and commit)
```

### Assets (CSS & JS)
* [Compiling Assets (CSS & JS)](./docs/compiling-assets-css-js.md).
* [Asset Libraries (CSS & JS)](./docs/asset-libraries.md).

## Configuration Management
* [Config Management](./docs/config-management.md).

## Server/Production Use
See [Deployment Notes](./docs/deployment.md).

## Database Backup/Restoration
See [Database Backup/Restoration Notes](./docs/database.md).

## Single Sign On / SAML
See [SAML/SSO Notes](./docs/saml-sso.md).

## Logging
Developers and DevOps can access logs for the application using:  

### Kibana/ElasticSearch
Navigate to https://event-logging.lib.duke.edu  

### Directly Using Docker
```sh
$ ssh dulcet-staging.lib.duke.edu
$ docker logs -f drupal9_app
```

## DevOps Considerations
### Firewalls
```sh
$ [sudo] firewall-cmd --add-port=80/tcp --zone=<yourzone> [--permanent]
$ [sudo] firewall-cmd --add-port=443/tcp --zone=<yourzone> [--permanent]
$ [sudo] firewall-cmd --reload # do this if you used --permanent flag
```

It is important to not use `--add-service=http|https` because Apache/HTTPD is not running locally on the target host.

## External Resources / More Info
* [Bitnami Drupal Documentation](https://github.com/bitnami/bitnami-docker-drupal)
* [How To Create a View Programmatically](http://subhojit777.in/create-views-programatically-drupal8/)

