#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

# Tell Drupal we're running behind a reverse proxy
# 
drupal_conf_set "\$settings['reverse_proxy']" "TRUE" yes
drupal_conf_set "\$settings['reverse_proxy_addresses']" "array(\$_SERVER['REMOTE_ADDR'])" yes
drupal_conf_set "\$settings['reverse_proxy_trusted_headers']" "\\\Symfony\\\Component\\\HttpFoundation\\\Request::HEADER_X_FORWARDED_ALL" yes

# Change config directory from default. See:
# https://www.drupal.org/docs/configuration-management/changing-the-storage-location-of-the-sync-directory
drupal_conf_set "\$settings['config_sync_directory']" "/opt/dul/config"

