#!/bin/bash
#
# 11-install-modules
# Purpose: ensure modules are installed (enabled)
# -- particularly the 'pathauto' module, which is
#    left disabled in the bitnami/drupal provisioning process

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

if [ -v DRUPAL_SKIP_BOOTSTRAP ] && [[ "$DRUPAL_SKIP_BOOTSTRAP" =~ ^(yes|YES) ]]; then
  info "11-install-modules: skipping..."
  exit
fi

info "Installing/enabling pathauto..."
# I think because pathauto has dependencies (token, ctools)
# it is not enabled using the DRUPAL_ENABLE_MODULES option.
# So, force the enable here.
# (This is only a guess -- dlc32)
drush en pathauto -y

# Configuration management modules:
info "Installing/enabling config modules..."
drush en config_direct_save -y
drush en config_ignore -y

# Layout modules:
info "Installing/enabling layout modules..."
drush en layout_builder -y
drush en layout_discovery -y
drush en field_layout -y
drush en exclude_node_title -y

# Media modules:
info "Installing/enabling media modules..."
drush en media -y
drush en media_library -y
drush en views_bootstrap -y
drush en flexslider -y
drush en flexslider_views -y
drush en flexslider_fields -y

# Asset modules:
info "Installing/enabling asset modules..."
drush en asset_injector -y

info "Enabling aggregator..."
drush en aggregator -y

# JSON Views Source module:
info "Installing/enabling JSON Views Source module..."
drush en views_json_source -y

# Stats & Analytics:
info "Installing/enabling Google Analytics module..."
drush en google_analytics -y
