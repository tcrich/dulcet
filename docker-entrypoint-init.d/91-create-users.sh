#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

if [[ "$BYPASS_IMPORT" =~ ^(yes|YES|true|TRUE) ]]; then
  exit
fi

if [ ! -v CREATE_USER_ACCOUNTS ]; then
  exit
fi

if ! [[ "$CREATE_USER_ACCOUNTS" =~ ^(yes|YES|true|TRUE) ]]; then
  exit
fi

# these functions will attempt to detect
# if the 'samlauth' module is enabled.
# If it is, then link the created accounts
info "[91-create-users] Creating user accounts for WEB members..."
drush cwu

info "[91-create-users] Creating user accounts for developers..."
drush cdevs
