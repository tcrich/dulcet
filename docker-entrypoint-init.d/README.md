# Entrypoint Scripts

The scripts in this directory will run during the initialization process of the Drupal container.  
  
These scripts will run in order, by name. To that end, the entries begin with a two-digit number, followed 
by the intended task.
  
The intended use case is to use `drush` to configure Drupal to DUL's liking.
  
If you're interested in the "under-the-hood" aspects, see this [post-init.sh](https://github.com/bitnami/bitnami-docker-drupal/blob/master/9/debian-10/rootfs/post-init.sh) file from Bitnami's Drupal repository.

## Creating Your Script
Create a file in the `docker-entrypoint-init.d/` directory, with a naming convention of `XX-my-task.sh` (where XX is a two-digit number).
  
Then...
```sh
#!/bin/bash

# load logging library from Bitnami's setup
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

whoam

drush config-set "my.category" mysetting "MY_VALUE"

# info is imported from the liblog library
info "Done!"
```
  
## Set The Executable Bit
When done, make sure to `chmod a+x` on the file so it'll run during Bitnami's setup.
