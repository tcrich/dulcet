
#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

info "[Site Settings] Setting site name to '$DRUPAL_SITE_NAME'..."
drush cset system.site name "$DRUPAL_SITE_NAME" -y

info "[Site Settings] Setting site email to '$DRUPAL_EMAIL'..."
drush cset system.site mail $DRUPAL_EMAIL -y

