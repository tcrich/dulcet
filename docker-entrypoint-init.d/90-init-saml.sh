#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

if [ ! -z "$ENABLE_SAML" ] && [ "$ENABLE_SAML" = "yes" ]; then
  # enable the samlauth module
  info "Install/enabling samlauth module..."
  drush en samlauth -y

  # create /opt/dul/static.yml with the contents below.
  cat <<- EOF > /opt/dul/static.yml
contact__site_page:
  menu_name: footer
  weight: 0
samlauth__saml_controller_logout__logout:
  enabled: true
  menu_name: account
  parent: ''
  expanded: false
  weight: 11
user__logout:
  enabled: false
  menu_name: account
  parent: ''
  weight: 10
  expanded: false
EOF

  read -r -d '' SAMLAUTH_MAP_USERS_ROLES <<'EOF'
administrator: administrator
webeditor: webeditor
EOF

  METADATA_FILE='/opt/dul/duke-metadata-2-signed.xml'
  wget -O /opt/dul/duke-metadata-2-signed.xml https://shib.oit.duke.edu/duke-metadata-2-signed.xml
  if [ -f "$METADATA_FILE" ]; then
    info "[SAML] Inspecting the IdP metadata..."
    IDP_CERT=`xpath -q -e '//EntityDescriptor/IDPSSODescriptor/KeyDescriptor[1]/ds:KeyInfo/ds:X509Data/ds:X509Certificate/text()' /opt/dul/duke-metadata-2-signed.xml`
    IDP_ENTITYID=`xpath -s "" -p "" -q -e 'string(//EntityDescriptor/@entityID)' /opt/dul/duke-metadata-2-signed.xml`
    IDP_SSO_SERVICE=`xpath -s "" -p "" -q -e 'string(//EntityDescriptor/IDPSSODescriptor/SingleSignOnService[@Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"]/@Location)' /opt/dul/duke-metadata-2-signed.xml`

    info $IDP_ENTITYID
    info $IDP_SSO_SERVICE
    info ${IDP_CERT//^\n/}

    if [ ! -z "$SP_ENTITY_ID" ]; then
      info "[SAML] Setting SP Entity ID to '$SP_ENTITY_ID'..."
      drush cset samlauth.authentication sp_entity_id $SP_ENTITY_ID -y
    fi

    info "[SAML] Allow users to be created from SAML data..."
    drush cset samlauth.authentication create_users true -y

    info "[SAML] Use 'uid' SAML attribute as Drupal account username..."
    drush cset samlauth.authentication user_name_attribute uid -y

    info "[SAML] Map 'mail' attribute to user's email field..."
    drush cset samlauth.authentication user_mail_attribute mail -y

    info "[SAML] Setting unique_id attribute..."
    drush config-set samlauth.authentication unique_id_attribute eduPersonPrincipalName -y

    info "[SAML] Ensure existing local accounts are linked by 'username'..."
    drush cset samlauth.authentication map_users_name true -y

    info "[SAML] Ensure email addresses are synchronized..."
    drush cset samlauth.authentication sync_mail true -y

    info "[SAML] Mapping (allowed) user roles..."
    drush cset -y --input-format=yaml samlauth.authentication map_users_roles "$SAMLAUTH_MAP_USERS_ROLES"

    info "[SAML] Set Metadata validaty to 2 days..."
    drush cset -y samlauth.authentication metadata_valid_secs 172800

    info "[SAML] Setting location of X509 certificate..."
    drush config-set samlauth.authentication sp_x509_certificate 'file:/sp/sp.pem' -y

    info "[SAML] Setting location of private key..."
    drush config-set samlauth.authentication sp_private_key 'file:/sp/sp.key' -y

    info "[SAML] Setting IdP's EntityID to '$IDP_ENTITYID"
    drush config-set samlauth.authentication idp_entity_id $IDP_ENTITYID -y

    info "[SAML] Setting SSO Sign In Service..."
    drush config-set samlauth.authentication idp_single_sign_on_service $IDP_SSO_SERVICE -y

    info "[SAML] Setting SSO Logout Service..."
    drush config-set samlauth.authentication idp_single_log_out_service 'https://shib.oit.duke.edu/cgi-bin/logout.pl' -y

    info "[SAML] Setting IdP certificate..."
    echo [${IDP_CERT//^\n/}] | drush config-set -y --input-format=yaml samlauth.authentication idp_certs -
  fi

  cat /opt/dul/static.yml | drush cset -y core.menu.static_menu_link_overrides definitions --input-format=yaml -
fi
