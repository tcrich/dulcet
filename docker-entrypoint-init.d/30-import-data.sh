#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

if [ ! -d "$DUL_DRUPAL7_EXPORT_DIR" ]; then
  info "[30-import-data] $DUL_DRUPAL7_EXPORT_DIR doesn't exist. Moving on..."
  exit
fi

if [ -v BYPASS_IMPORT ] && [ "$BYPASS_IMPORT" == "yes" ]; then
  info "[30-import-data] Bypassing all imports..."
  exit
fi

# Load all of the nodes/content types/files from our
# migrated Drupal7 data
# Use this to create block types
info "Creating content-types..."
drush dul-ctypes

info "Creating taxomony tags for carousel content..."
drush tax-in $DUL_DRUPAL7_EXPORT_DIR

info "Creating 'reconcile' folders..."
mkdir /opt/dul/reconcile
mkdir /opt/dul/reconcile/perkins

info "[Perkins] Importing menu tree and feeds"
drush dul-menus perkins
drush dul-feeds perkins

## NOTE --order is important
## import the blocks and managed files (images) before node creation
info "Importing custom blocks from Perkins site"
drush blocks-in --block-type perkins_block $DUL_DRUPAL7_EXPORT_DIR/perkins/blocks/custom --drup7_site perkins
drush blocks-in --block-type perkins_block $DUL_DRUPAL7_EXPORT_DIR/new_content/blocks --drup7_site perkins

drush file-in $DUL_DRUPAL7_EXPORT_DIR/perkins/file_managed
drush node-in $DUL_DRUPAL7_EXPORT_DIR/perkins/node --reconcile-menulinks --drup7_site perkins
info "[Perkins] Creating skeleton top-level pages from Panel Pages..."
drush node-in $DUL_DRUPAL7_EXPORT_DIR/perkins/page_manager --drup7_site perkins

if [ -d "$DUL_DRUPAL7_EXPORT_DIR/new_content/node" ]; then
  info "Importing nodes with new styleguide applied..."
  drush node-in $DUL_DRUPAL7_EXPORT_DIR/new_content/node --no-adjust
fi

# import the converted module/system blocks that were also 
# used as asidebox content
info "Importing converted module/system blocks used as asidebox content..."
drush abox-import $DUL_DRUPAL7_EXPORT_DIR/asideblocks-perkins.json

## COMING SOON TODO
#drush node-in --content-type exhibits $DUL_DRUPAL7_EXPORT_DIR/exhibits

if [ "$IMPORT_ALL_DATA" == "TRUE" ] || [ "$IMPORT_ALL_DATA" == "true" ] || [ "$IMPORT_ALL_DATA" == "yes" ] || [ "$IMPORT_ALL_DATA" == "YES" ]; then

  mkdir /opt/dul/reconcile/rubenstein
  mkdir /opt/dul/reconcile/datavis
  mkdir /opt/dul/reconcile/east

  info "[Rubenstein] Importing menu tree and feeds..."
  drush dul-menus --prefix rl --link-prefix rubenstein rubenstein
  drush dul-feeds rubenstein

  info "[Datavis] Importing menu tree and feeds..."
  drush dul-menus --prefix data --link-prefix data datavis
  drush dul-feeds datavis

  info "[East] Importing menu tree and feeds..."
  drush dul-menus --prefix east east
  drush dul-feeds east

  drush file-in $DUL_DRUPAL7_EXPORT_DIR/rubenstein/file_managed
  drush blocks-in --block-type rubenstein_block --title-prefix RL $DUL_DRUPAL7_EXPORT_DIR/rubenstein/blocks/custom --drup7_site rubenstein
  drush node-in --content-type rubenstein_page --menu-prefix rl $DUL_DRUPAL7_EXPORT_DIR/rubenstein/node  --reconcile-menulinks --drup7_site rubenstein
  info "[Rubenstein] Creating skeleton top-level pages from Panel Pages..."
  drush node-in --menu-prefix rl --content-type rubenstein_page $DUL_DRUPAL7_EXPORT_DIR/rubenstein/page_manager --drup7_site rubenstein

  drush file-in $DUL_DRUPAL7_EXPORT_DIR/datavis/file_managed
  drush blocks-in --block-type datavis_block --title-prefix Data $DUL_DRUPAL7_EXPORT_DIR/datavis/blocks/custom --drup7_site datavis
  drush node-in --content-type datavis_page $DUL_DRUPAL7_EXPORT_DIR/datavis/node --reconcile-menulinks --drup7_site datavis
  info "[Data] Creating skeleton top-level pages from Panel Pages..."
  drush node-in --menu-prefix data --content-type datavis_page $DUL_DRUPAL7_EXPORT_DIR/datavis/page_manager --drup7_site datavis

  drush file-in $DUL_DRUPAL7_EXPORT_DIR/east/file_managed
  drush blocks-in --block-type east_block --title-prefix East $DUL_DRUPAL7_EXPORT_DIR/east/blocks/custom

  drush node-in --content-type lilly_page $DUL_DRUPAL7_EXPORT_DIR/east/lilly/node --reconcile-menulinks --drup7_site east
  drush node-in --content-type music_page $DUL_DRUPAL7_EXPORT_DIR/east/music/node --reconcile-menulinks --drup7_site east
  info "[Lilly/Music] Creating skeleton top-level pages from Panel Pages..."
  drush node-in $DUL_DRUPAL7_EXPORT_DIR/east/page_manager --drup7_site east
fi

if [[ $USE_DUL_FRONTPAGE =~ (YES|yes|TRUE|true) ]]; then
  info "Setting the front page to /home..."
  drush cset -y system.site page.front "/home"
fi

#info "removing 'reconcile' folders..."
#rm -rf /opt/dul/reconcile
# drush node-in $DUL_DRUPAL7_EXPORT_DIR/east/node

