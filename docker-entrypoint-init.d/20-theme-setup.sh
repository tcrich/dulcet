#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

if [ -v DRUPAL_SKIP_BOOTSTRAP ] && [[ "$DRUPAL_SKIP_BOOTSTRAP" =~ ^(yes|YES) ]]; then
  info "20-theme-setup: skipping..."
  exit
fi

info "Enable Dulcet theme..."

drush theme:enable radix -y

drush theme:enable drupal9_dulcet -y
drush config-set system.theme default drupal9_dulcet -y

info "done!"

