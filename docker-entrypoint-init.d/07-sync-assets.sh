#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

if [ -v BYPASS_ASSETS_SYNC ] && [ "$BYPASS_ASSETS_SYNC" == "yes" ]; then
  info "[07-copy-assets] Bypassing assets sync..."
  exit
fi

if [ ! -d "/files-sync" ]; then
  info "[07-sync-assets] cannot find /files-sync in the container."
  info "[07-sync-assets] Have you mounted a directory to this location?"
  info "[07-sync-assets] volumes:"
  info "[07-sync-assets]   - '/path/to/drupal/default/files:/files-sync' (or)"
  info "[07-sync-assets]   - './files-to-sync:/files-sync'"
  exit
fi

if [ ! -w "/bitnami/drupal/sites/default/files" ]; then
  info "[07-sync-assets] /bitnami/drupal/sites/default/files is not writable! Exiting..."
fi

info "[07-sync-assets] syncing drupal 'default' files..."
ls -la /bitnami/sites/default/files
rsync -rav --ignore-errors /files-sync/* /bitnami/drupal/sites/default/files/

info "[07-sync-assets] done :)"
