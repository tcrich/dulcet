#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

info "Rebuilding Drupal cache..."
drush cr

info "Done with DUL-specific initialization! Happy Drupal-ing!"

