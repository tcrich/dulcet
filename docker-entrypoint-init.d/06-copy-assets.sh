#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

if [ -v BYPASS_IMPORT ] && [ "$BYPASS_IMPORT" == "yes" ]; then
  info "[06-copy-assets] Bypassing assets imports..."
  exit
fi

if [[ -f "/bitnami/drupal/.user_scripts_initialized" ]]; then
  exit
fi

info "06-copy-assets: copying assets from Drupal 7 instance..."

if [ -d "/opt/dul/default-site" ]; then
  if [ -f "/opt/dul/default-site/files/dul/dul.tgz" ]; then
    info "Copying known image assets for DUL..."
    mkdir /bitnami/drupal/sites/default/files/dul
    tar -xzvf /opt/dul/default-site/files/dul/dul.tgz -C /bitnami/drupal/sites/default/files/dul/
  fi

  if [ -f "/opt/dul/default-site/files/rubenstein/rubenstein.tgz" ]; then
    info "Copying known image assets for Rubenstein..."
    mkdir /bitnami/drupal/sites/default/files/rubenstein
    tar -xzvf /opt/dul/default-site/files/rubenstein/rubenstein.tgz -C /bitnami/drupal/sites/default/files/rubenstein/
  fi

  if [ -f "/opt/dul/default-site/files/datavis/datavis.tgz" ]; then
    info "Copying known image assets for Datavis..."
    mkdir /bitnami/drupal/sites/default/files/datavis
    tar -xzvf /opt/dul/default-site/files/datavis/datavis.tgz -C /bitnami/drupal/sites/default/files/datavis/
  fi

  if [ -f "/opt/dul/default-site/files/east/east.tgz" ]; then
    info "Copying known image assets for East..."
    mkdir /bitnami/drupal/sites/default/files/east
    tar -xzvf /opt/dul/default-site/files/east/east.tgz -C /bitnami/drupal/sites/default/files/east/
  fi
fi
