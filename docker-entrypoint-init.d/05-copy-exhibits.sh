#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

# DEPRECATED script no longer used in entrypoint process

#if [ ! -v IMPORT_EXHIBITS ]; then
#  exit
#fi

#if ! [[ "$IMPORT_EXHIBITS" =~ ^(yes|true|YES|TRUE) ]]; then
#  exit
#fi

#info "Copying 'Exhibits' image files..."
#cp -r /opt/dul/exhibits /bitnami/drupal/sites/default/files/

